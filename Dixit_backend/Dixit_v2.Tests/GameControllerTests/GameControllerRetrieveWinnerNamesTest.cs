﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerRetrieveWinnerNamesTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public void CanRetrieveWinnerNameWhenOneWinner()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<string>> result;
            var expectedResult = new List<string>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                int winnerId = util.MockFirstRoundWithOnePlayerGainMaxPointsPossible();
                var gameId = util.RetrieveValidGameId();
                expectedResult.Add(util.RetrievePlayerName(winnerId));
                result = sut.RetrieveWinnerNames(gameId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<string>>(okResult.Value).OrderBy(m => m);
            var orderedExpectedResult = expectedResult.OrderBy(m => m);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        [Fact]
        public void CanRetrieveWinnerNameWhenMultipleWinners()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<string>> result;
            var expectedResult = new List<string>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int roundId = util.RetrieveValidRoundId();
                int storytellerId = util.RetrieveStorytellerId(roundId);
                util.MockSubsequentRoundWhenAllGuessed(storytellerId);
                util.MockSubsequentRoundWhenAllGuessed(storytellerId);
                var nonStorytellerPlayers = util.RetrievePlayerIdsWithoutStoryteller(roundId);
                nonStorytellerPlayers.ForEach(m => expectedResult.Add(util.RetrievePlayerName(m)));
                var gameId = util.RetrieveValidGameId();
                result = sut.RetrieveWinnerNames(gameId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<string>>(okResult.Value).OrderBy(m => m);
            var orderedExpectedResult = expectedResult.OrderBy(m => m);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        [Fact]
        public void CannotRetrieveWinnerNameWhenInvalidGameId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<string>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFirstRoundWithOnePlayerGainMaxPointsPossible();
                var invalidGameId = util.GenerateInvalidGameId();
                result = sut.RetrieveWinnerNames(invalidGameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrieveWinnerNameWhenGameIsNotOver()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<string>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithNoneGuessedStorytellerCard();
                var gameId = util.RetrieveValidGameId();
                result = sut.RetrieveWinnerNames(gameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
