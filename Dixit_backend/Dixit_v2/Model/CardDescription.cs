﻿using System.ComponentModel.DataAnnotations;

namespace Dixit_v2.Model
{
    public class CardDescription
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public int RoundId { get; set; }
    }
}
