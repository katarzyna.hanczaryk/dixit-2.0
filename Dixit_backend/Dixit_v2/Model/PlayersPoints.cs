﻿namespace Dixit_v2.Model
{
    public class PlayersPoints
    {
        public string PlayerName { get; set; }
        public int RoundPoints { get; set; }
        public int GamePoints { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is PlayersPoints)) return false;

            return (this.PlayerName == ((PlayersPoints)obj).PlayerName
                && this.RoundPoints == ((PlayersPoints)obj).RoundPoints
                && this.GamePoints == ((PlayersPoints)obj).GamePoints);
        }

        public override int GetHashCode()
        {
            return PlayerName.GetHashCode()
                ^ RoundPoints.GetHashCode()
                ^ GamePoints.GetHashCode();
        }
    }
}
