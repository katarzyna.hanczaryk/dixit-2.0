﻿using Dixit_v2.Common;
using Dixit_v2.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Dixit_v2.Controllers
{
    public class PlayerController : BaseApiController<PlayerController>
    {
        public PlayerController(ApplicationDbContext db, ILoggerFactory loggerFactory, IConfiguration iconfig) : base(db, loggerFactory, iconfig)
        {
        }

        [HttpPost("api/player")]
        public async Task<ActionResult<int>> CreatePlayerAsync([FromBody] PlayerName playerName)
        {
            try
            {
                int playerId = await AddNewPlayerAsync(playerName.Name);
                return Ok(playerId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Adding a new player failed");
                return StatusCode(500, "New player was not added");
            }
        }

        [HttpGet("api/player/{playerId}")]
        public async Task<ActionResult<AssignedGameAndRound>> AssignGameAndRoundAsync(int playerId)
        {
            try
            {
                var result = await AssignToGameAsync(playerId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Assigning game or round to a player failed");
                return StatusCode(500, "Player has not been assigned to any game and round");
            }
        }

        [HttpGet("api/round/{roundId}/storytellerId")]
        public ActionResult<int> RetrieveStorytellerId(int roundId)
        {
            try
            {
                var result = GetCurrentStorytellerId(roundId);
                if (result > 0)
                    return Ok(result);
                throw new Exception("StortytellerId for given round has not been found in db");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"StorytellerId for round {roundId} has not been retrieved");
                return StatusCode(500, "StorytellerId has not been found");
            }
        }

        [HttpGet("api/round/{roundId}/player/{playerId}/playerPoints")]
        public ActionResult<int> RetrievePlayerRoundPoints(int roundId, int playerId)
        {
            try
            {
                var result = GetPlayerRoundPoints(roundId, playerId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Player round points for player {playerId} and round {roundId} have not been retrieved");
                return StatusCode(500, "Player round points have not been retrieved");
            }
        }

        [HttpGet("api/round/{newRoundId}/player/{playerId}/isStoryteller")]
        public ActionResult<bool> IsPlayerStoryteller(int newRoundId, int playerId)
        {
            try
            {
                var result = IsStoryteller(newRoundId, playerId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"The info whether the player {playerId} is a storyteller hasn't been retrieved.");
                return StatusCode(500, "The info whether player is a storyteller has not been retrieved");
            }
        }
    }
}
