﻿using System.ComponentModel.DataAnnotations;

namespace Dixit_v2.Model
{
    public class Round
    {
        [Key]
        public int Id { get; set; }
        public int StorytellerId { get; set; }
    }
}
