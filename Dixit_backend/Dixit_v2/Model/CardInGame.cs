﻿using System.ComponentModel.DataAnnotations;

namespace Dixit_v2.Model
{
    public class CardInGame
    {
        [Key]
        public int Id { get; set; }
        public int CardId { get; set; }
        public int PlayerId { get; set; }
    }
}
