﻿using Microsoft.EntityFrameworkCore;

namespace Dixit_v2.Model
{
    [Keyless]
    public class DataForPointsCalculation
    {
        public int GameId { get; set; }
        public int RoundId { get; set; }
        public int StorytellerId { get; set; }
        public int CardId { get; set; }
        public int? SelectedByPlayer { get; set; }
        public string Name { get; set; }
        public int? GuessedCardId { get; set; }
    }
}
