﻿using Dixit_v2.Controllers;
using Dixit_v2.Model;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerGetPlayersRoundAndGamePointsTests : IntegrationTests
    {
        private Util util = new();

        [Fact]
        public void CanRetrievePoints_OneRound_AllPlayersGuessedStorytellerCard()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<PlayersPoints>> result;
            var expectedResult = new List<PlayersPoints>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                result = sut.GetPlayersRoundAndGamePoints(gameId);
                int roundId = util.RetrieveValidRoundId();
                int baseMediumPoints = util.RetrieveBaseMediumPoints();
                int baseMinimumPoints = util.RetrieveBaseMinimumPoints();
                var nonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(roundId).ToList();
                nonStorytellerPlayerIds.ForEach(m =>
                                expectedResult.Add(
                                    new PlayersPoints()
                                    {
                                        PlayerName = util.RetrievePlayerName(m),
                                        RoundPoints = baseMediumPoints,
                                        GamePoints = baseMediumPoints
                                    }));
                var storytellerId = util.RetrieveStorytellerId(roundId);
                expectedResult.Add(new PlayersPoints()
                {
                    PlayerName = util.RetrievePlayerName(storytellerId),
                    RoundPoints = baseMinimumPoints,
                    GamePoints = baseMinimumPoints
                });
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<PlayersPoints>>(okResult.Value).OrderBy(m => m.PlayerName);
            var orderedExpectedResult = expectedResult.OrderBy(m => m.PlayerName);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        [Fact]
        public void CannotRetrievePoints_OneRound_AllPlayersGuessedStorytellerCard_InvalidGameId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<PlayersPoints>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int invalidGameId = util.GenerateInvalidGameId();
                result = sut.GetPlayersRoundAndGamePoints(invalidGameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CanRetrievePoints_OneRound_NonePlayersGuessedStorytellerCard()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<PlayersPoints>> result;
            var expectedResult = new List<PlayersPoints>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithNoneGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                result = sut.GetPlayersRoundAndGamePoints(gameId);
                int roundId = util.RetrieveValidRoundId();
                int baseMediumPoints = util.RetrieveBaseMediumPoints();
                int baseMinimumPoints = util.RetrieveBaseMinimumPoints();
                var nonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(roundId).ToList();
                nonStorytellerPlayerIds.ForEach(m =>
                                expectedResult.Add(
                                    new PlayersPoints()
                                    {
                                        PlayerName = util.RetrievePlayerName(m),
                                        RoundPoints = baseMediumPoints,
                                        GamePoints = baseMediumPoints
                                    }));
                var storytellerId = util.RetrieveStorytellerId(roundId);
                expectedResult.Add(new PlayersPoints()
                {
                    PlayerName = util.RetrievePlayerName(storytellerId),
                    RoundPoints = baseMinimumPoints,
                    GamePoints = baseMinimumPoints
                });
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<PlayersPoints>>(okResult.Value).OrderBy(m => m.PlayerName);
            var orderedExpectedResult = expectedResult.OrderBy(m => m.PlayerName);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        [Fact]
        public void CanRetrievePoints_OneRound_OnePlayersGuessedStorytellerCard()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<PlayersPoints>> result;
            var expectedResult = new List<PlayersPoints>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithOneGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                result = sut.GetPlayersRoundAndGamePoints(gameId);
                int roundId = util.RetrieveValidRoundId();
                int baseMaximumPoints = util.RetrieveBaseMaxPoints();
                int baseMinimumPoints = util.RetrieveBaseMinimumPoints();
                int extraSinglePoints = util.RetrieveExtraSinglePoints();
                var nonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(roundId).ToList();

                int currentBasePoints = baseMaximumPoints;
                int currentExtraPoints = extraSinglePoints;
                int nonStorytellerPlayerCount = nonStorytellerPlayerIds.Count;
                for (int i = 0; i < nonStorytellerPlayerCount; i++)
                {
                    var playerId = nonStorytellerPlayerIds[i];
                    int currentTotalPoints = currentBasePoints + currentExtraPoints;
                    var currentPlayerPoints = new PlayersPoints()
                    {
                        PlayerName = util.RetrievePlayerName(playerId),
                        RoundPoints = currentTotalPoints,
                        GamePoints = currentTotalPoints
                    };
                    expectedResult.Add(currentPlayerPoints);
                    currentBasePoints = 0;
                    currentExtraPoints = i == nonStorytellerPlayerCount - 2 ? 0 : extraSinglePoints;
                }

                var storytellerId = util.RetrieveStorytellerId(roundId);
                expectedResult.Add(new PlayersPoints()
                {
                    PlayerName = util.RetrievePlayerName(storytellerId),
                    RoundPoints = baseMaximumPoints,
                    GamePoints = baseMaximumPoints
                });
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<PlayersPoints>>(okResult.Value).OrderBy(m => m.PlayerName);
            var orderedExpectedResult = expectedResult.OrderBy(m => m.PlayerName);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        [Fact]
        public void CanRetrievePoints_OneRound_TwoPlayersGuessedStorytellerCard()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<PlayersPoints>> result;
            var expectedResult = new List<PlayersPoints>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithTwoGuessedStorytellerCards();
                int gameId = util.RetrieveValidGameId();
                result = sut.GetPlayersRoundAndGamePoints(gameId);
                int roundId = util.RetrieveValidRoundId();
                int baseMaximumPoints = util.RetrieveBaseMaxPoints();
                int baseMinimumPoints = util.RetrieveBaseMinimumPoints();
                int extraSinglePoints = util.RetrieveExtraSinglePoints();
                var nonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(roundId).ToList();

                int currentBasePoints = baseMaximumPoints;
                int currentExtraPoints = extraSinglePoints;
                int nonStorytellerPlayerCount = nonStorytellerPlayerIds.Count;
                expectedResult.Add(new PlayersPoints()
                {
                    PlayerName = util.RetrievePlayerName(nonStorytellerPlayerIds[0]),
                    RoundPoints = currentBasePoints,
                    GamePoints = currentBasePoints
                });
                for (int i = 1; i < nonStorytellerPlayerCount; i++)
                {
                    var playerId = nonStorytellerPlayerIds[i];
                    int currentTotalPoints = currentBasePoints + currentExtraPoints;
                    var currentPlayerPoints = new PlayersPoints()
                    {
                        PlayerName = util.RetrievePlayerName(playerId),
                        RoundPoints = currentTotalPoints,
                        GamePoints = currentTotalPoints
                    };
                    expectedResult.Add(currentPlayerPoints);
                    currentBasePoints = 0;
                    currentExtraPoints = i == nonStorytellerPlayerCount - 2 ? 0 : extraSinglePoints;
                }

                var storytellerId = util.RetrieveStorytellerId(roundId);
                expectedResult.Add(new PlayersPoints()
                {
                    PlayerName = util.RetrievePlayerName(storytellerId),
                    RoundPoints = baseMaximumPoints,
                    GamePoints = baseMaximumPoints
                });
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<PlayersPoints>>(okResult.Value).OrderBy(m => m.PlayerName);
            var orderedExpectedResult = expectedResult.OrderBy(m => m.PlayerName);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        //first round -> one person guess the card
        //second round -> every person guessed the storyteller card
        [Fact]
        public void CanRetrievePoints_TwoRounds_EveryPlayerGuessedStorytellerCard()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<PlayersPoints>> result;
            var expectedResult = new List<PlayersPoints>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithOneGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                int firstRoundId = util.RetrieveValidRoundId();
                int baseMaximumPoints = util.RetrieveBaseMaxPoints();
                int baseMinimumPoints = util.RetrieveBaseMinimumPoints();
                int extraSinglePoints = util.RetrieveExtraSinglePoints();
                int baseMediumPoints = util.RetrieveBaseMediumPoints();

                // first round - points calculation
                var nonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(firstRoundId).ToList();

                int currentBasePoints = baseMaximumPoints;
                int currentExtraPoints = extraSinglePoints;
                int nonStorytellerPlayerCount = nonStorytellerPlayerIds.Count;
                for (int i = 0; i < nonStorytellerPlayerCount; i++)
                {
                    var playerId = nonStorytellerPlayerIds[i];
                    int currentTotalPoints = currentBasePoints + currentExtraPoints;
                    var currentPlayerPoints = new PlayersPoints()
                    {
                        PlayerName = util.RetrievePlayerName(playerId),
                        RoundPoints = currentTotalPoints,
                        GamePoints = currentTotalPoints
                    };
                    expectedResult.Add(currentPlayerPoints);
                    currentBasePoints = 0;
                    currentExtraPoints = i == nonStorytellerPlayerCount - 2 ? 0 : extraSinglePoints;
                }

                var firstStorytellerId = util.RetrieveStorytellerId(firstRoundId);
                expectedResult.Add(new PlayersPoints()
                {
                    PlayerName = util.RetrievePlayerName(firstStorytellerId),
                    RoundPoints = baseMaximumPoints,
                    GamePoints = baseMaximumPoints
                });

                util.MockSecondRoundWhereEveryPersonGuessed();
                // second round - points calculation
                var secondRoundId = util.RetrieveValidRoundId();
                var secondStorytellerId = util.RetrieveStorytellerId(secondRoundId);
                string secondStorytellerName = util.RetrievePlayerName(secondStorytellerId);
                foreach (var item in expectedResult)
                {
                    if (item.PlayerName == secondStorytellerName)
                        item.RoundPoints = baseMinimumPoints;
                    else
                    {
                        item.RoundPoints = baseMediumPoints;
                        item.GamePoints += baseMediumPoints;
                    }
                }
                result = sut.GetPlayersRoundAndGamePoints(gameId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<PlayersPoints>>(okResult.Value).OrderBy(m => m.PlayerName);
            var orderedExpectedResult = expectedResult.OrderBy(m => m.PlayerName);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        //first round -> one person guess the card
        //second round -> nobody guessed the storyteller card
        [Fact]
        public void CanRetrievePoints_TwoRounds_NonePlayerGuessedStorytellerCard()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<PlayersPoints>> result;
            var expectedResult = new List<PlayersPoints>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithOneGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                int firstRoundId = util.RetrieveValidRoundId();
                int baseMaximumPoints = util.RetrieveBaseMaxPoints();
                int baseMinimumPoints = util.RetrieveBaseMinimumPoints();
                int extraSinglePoints = util.RetrieveExtraSinglePoints();
                int baseMediumPoints = util.RetrieveBaseMediumPoints();

                // first round - points calculation
                var nonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(firstRoundId).ToList();

                int currentBasePoints = baseMaximumPoints;
                int currentExtraPoints = extraSinglePoints;
                int nonStorytellerPlayerCount = nonStorytellerPlayerIds.Count;
                for (int i = 0; i < nonStorytellerPlayerCount; i++)
                {
                    var playerId = nonStorytellerPlayerIds[i];
                    int currentTotalPoints = currentBasePoints + currentExtraPoints;
                    var currentPlayerPoints = new PlayersPoints()
                    {
                        PlayerName = util.RetrievePlayerName(playerId),
                        RoundPoints = currentTotalPoints,
                        GamePoints = currentTotalPoints
                    };
                    expectedResult.Add(currentPlayerPoints);
                    currentBasePoints = 0;
                    currentExtraPoints = i == nonStorytellerPlayerCount - 2 ? 0 : extraSinglePoints;
                }

                var firstStorytellerId = util.RetrieveStorytellerId(firstRoundId);
                expectedResult.Add(new PlayersPoints()
                {
                    PlayerName = util.RetrievePlayerName(firstStorytellerId),
                    RoundPoints = baseMaximumPoints,
                    GamePoints = baseMaximumPoints
                });

                util.MockSecondRoundWhereNobodyGuessed();
                // second round - points calculation
                var secondRoundId = util.RetrieveValidRoundId();
                var secondStorytellerId = util.RetrieveStorytellerId(secondRoundId);
                string secondStorytellerName = util.RetrievePlayerName(secondStorytellerId);
                foreach (var item in expectedResult)
                {
                    if (item.PlayerName == secondStorytellerName)
                        item.RoundPoints = baseMinimumPoints;
                    else
                    {
                        item.RoundPoints = baseMediumPoints;
                        item.GamePoints += baseMediumPoints;
                    }
                }
                result = sut.GetPlayersRoundAndGamePoints(gameId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<PlayersPoints>>(okResult.Value).OrderBy(m => m.PlayerName);
            var orderedExpectedResult = expectedResult.OrderBy(m => m.PlayerName);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        //first round - all players guessed
        // second round - one player guessed
        [Fact]
        public void CanRetrievePoints_TwoRounds_OnePlayerGuessedStorytellerCard()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<PlayersPoints>> result;
            var expectedResult = new List<PlayersPoints>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                int roundId = util.RetrieveValidRoundId();
                int baseMediumPoints = util.RetrieveBaseMediumPoints();
                int baseMinimumPoints = util.RetrieveBaseMinimumPoints();
                int baseMaximumPoints = util.RetrieveBaseMaxPoints();
                int extraSinglePoints = util.RetrieveExtraSinglePoints();
                // first round - points calculation
                var nonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(roundId)
                                                  .OrderBy(m => m)
                                                  .ToList();
                nonStorytellerPlayerIds.ForEach(m =>
                                expectedResult.Add(
                                    new PlayersPoints()
                                    {
                                        PlayerName = util.RetrievePlayerName(m),
                                        RoundPoints = baseMediumPoints,
                                        GamePoints = baseMediumPoints
                                    }));
                var storytellerId = util.RetrieveStorytellerId(roundId);
                expectedResult.Add(new PlayersPoints()
                {
                    PlayerName = util.RetrievePlayerName(storytellerId),
                    RoundPoints = baseMinimumPoints,
                    GamePoints = baseMinimumPoints
                });
                util.MockSecondRoundWhereOnePlayerGuessedStorytellerCard();
                var secondRoundId = util.RetrieveValidRoundId();
                var secondStorytellerId = util.RetrieveStorytellerId(secondRoundId);
                var storytellerName = util.RetrievePlayerName(secondStorytellerId);

                var secondRoundNonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(roundId).ToList();
                var storytellerPoints = expectedResult.Where(m => m.PlayerName == storytellerName)
                                                      .FirstOrDefault();
                storytellerPoints.RoundPoints = baseMaximumPoints;
                storytellerPoints.GamePoints += baseMaximumPoints;
                var nonStorytellerPlayersList = util.RetrievePlayerIdsWithoutStoryteller(secondRoundId);
                var winnerPlayerName = util.RetrievePlayerName(nonStorytellerPlayersList[0]);
                var winnerPlayerPoints = expectedResult.Where(m => m.PlayerName == winnerPlayerName)
                                                       .FirstOrDefault();
                int winnerRoundPoints = baseMaximumPoints + (nonStorytellerPlayersList.Count - 1) * extraSinglePoints;
                winnerPlayerPoints.RoundPoints = winnerRoundPoints;
                winnerPlayerPoints.GamePoints += winnerRoundPoints;

                expectedResult.Where(m => m.PlayerName != winnerPlayerName && m.PlayerName != storytellerName)
                              .ToList()
                              .ForEach(n => n.RoundPoints = baseMinimumPoints);

                result = sut.GetPlayersRoundAndGamePoints(gameId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<PlayersPoints>>(okResult.Value).OrderBy(m => m.PlayerName);
            var orderedExpectedResult = expectedResult.OrderBy(m => m.PlayerName);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        //first round - all players guessed
        // second round - two players guessed
        [Fact]
        public void CanRetrievePoints_TwoRounds_TwoPlayersGuessedStorytellerCard()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<PlayersPoints>> result;
            var expectedResult = new List<PlayersPoints>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                int roundId = util.RetrieveValidRoundId();
                int baseMediumPoints = util.RetrieveBaseMediumPoints();
                int baseMinimumPoints = util.RetrieveBaseMinimumPoints();
                int baseMaximumPoints = util.RetrieveBaseMaxPoints();
                int extraSinglePoints = util.RetrieveExtraSinglePoints();
                // first round - points calculation
                var nonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(roundId)
                                                  .OrderBy(m => m)
                                                  .ToList();
                nonStorytellerPlayerIds.ForEach(m =>
                                expectedResult.Add(
                                    new PlayersPoints()
                                    {
                                        PlayerName = util.RetrievePlayerName(m),
                                        RoundPoints = baseMediumPoints,
                                        GamePoints = baseMediumPoints
                                    }));
                var storytellerId = util.RetrieveStorytellerId(roundId);
                expectedResult.Add(new PlayersPoints()
                {
                    PlayerName = util.RetrievePlayerName(storytellerId),
                    RoundPoints = baseMinimumPoints,
                    GamePoints = baseMinimumPoints
                });
                util.MockSecondRoundWhereTwoPlayersGuessedStorytellerCard();
                var secondRoundId = util.RetrieveValidRoundId();
                var secondStorytellerId = util.RetrieveStorytellerId(secondRoundId);
                var storytellerName = util.RetrievePlayerName(secondStorytellerId);

                var secondRoundNonStorytellerPlayerIds = util.RetrievePlayerIdsWithoutStoryteller(roundId).ToList();
                var storytellerPoints = expectedResult.Where(m => m.PlayerName == storytellerName)
                                                      .FirstOrDefault();
                storytellerPoints.RoundPoints = baseMaximumPoints;
                storytellerPoints.GamePoints += baseMaximumPoints;
                var nonStorytellerPlayersList = util.RetrievePlayerIdsWithoutStoryteller(secondRoundId);
                var winnerPlayerName = util.RetrievePlayerName(nonStorytellerPlayersList[0]);
                var winnerPlayerName2 = util.RetrievePlayerName(nonStorytellerPlayersList[1]);
                var winnerPlayerPoints = expectedResult.Where(m => m.PlayerName == winnerPlayerName)
                                                       .FirstOrDefault();
                var winnerPlayerPoints2 = expectedResult.Where(m => m.PlayerName == winnerPlayerName2)
                                                        .FirstOrDefault();
                int winnerRoundPoints = baseMaximumPoints + (nonStorytellerPlayersList.Count - 2) * extraSinglePoints;
                winnerPlayerPoints.RoundPoints = winnerRoundPoints;
                winnerPlayerPoints.GamePoints += winnerRoundPoints;
                winnerPlayerPoints2.RoundPoints = baseMaximumPoints;
                winnerPlayerPoints2.GamePoints += baseMaximumPoints;
                expectedResult.Where(m => m.PlayerName != winnerPlayerName && m.PlayerName != storytellerName
                                                                           && m.PlayerName != winnerPlayerName2)
                              .ToList()
                              .ForEach(n => n.RoundPoints = baseMinimumPoints);
                result = sut.GetPlayersRoundAndGamePoints(gameId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<PlayersPoints>>(okResult.Value).OrderBy(m => m.PlayerName);
            var orderedExpectedResult = expectedResult.OrderBy(m => m.PlayerName);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }
    }
}
