﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerGetGamePlayerNumberTests : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public async Task CanRetrievePlayerNumberForValidGameId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreateGameAndRound(util.CreatePlayer());
                int gameId = util.RetrieveValidGameId();
                result = await sut.GetGamePlayerNumber(gameId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var playersNumber = Assert.IsType<int>(okResult.Value);
            Assert.InRange(playersNumber, 1, 4);
        }

        [Fact]
        public async Task CannotRetrievePlayerForInvalidGameId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreateGameAndRound(util.CreatePlayer());
                int invalidGameId = util.GenerateInvalidRoundId();
                result = await sut.GetGamePlayerNumber(invalidGameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}

