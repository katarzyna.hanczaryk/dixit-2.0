import Vue from 'vue';
import Router from 'vue-router';
import App from './App.vue';
import NewUser from './components/NewUser.vue';
import WaitForPlayers from './components/WaitForPlayers.vue';
import PlayerPage from './components/PlayerPage.vue';
import StorytellerPage from './components/StorytellerPage.vue';
import SelectedCard from './components/SelectedCard.vue';
import SelectCardPlayerPage from './components/SelectCardPlayerPage.vue';
import WaitForOtherSelection from './components/WaitForOtherSelection.vue';
import RoundSelectedCards from './components/RoundSelectedCards.vue';
import WaitForOtherGuesses from './components/WaitForOtherGuesses.vue';
import RoundSummary from './components/RoundSummary.vue'
import EndOfGame from './components/EndOfGame.vue'

Vue.use(Router);
Vue.config.productionTip = true;
const router = new Router({
   routes: [
        {
            path: '/',
            name: 'home',
            component: NewUser
        },
        {
            path: '/player/:playerId',
            name: 'player',
            component: WaitForPlayers
        },
        {
            path: '/player/:playerId/game/:gameId/round/:roundId',
            name: 'playerPage',
            component: PlayerPage
        },
        {
            path: '/player/:playerId/game/:gameId/round/:roundId/card/:cardId',
            name: 'selectedCard',
            component: SelectedCard
        },
        {
            path: '/player/:playerId/game/:gameId/round/:roundId/storyteller',
            name: 'storytellerPage',
            component: StorytellerPage
        },
        {
            path: '/player/:playerId/game/:gameId/round/:roundId/selectCardPlayer',
            name: 'selectCardPlayerPage',
            component: SelectCardPlayerPage
        },
        {
            path: '/waitForOtherSelection/player/:playerId/game/:gameId/round/:roundId/card/:cardId',
            name: 'waitForOtherSelection',
            component: WaitForOtherSelection
        },
        {
            path: '/roundSelectedCards/player/:playerId/game/:gameId/round/:roundId/card/:cardId',
            name: 'roundSelectedCards',
            component: RoundSelectedCards
        },
        {
            path: '/roundGuessedCards/player/:playerId/game/:gameId/round/:roundId/card/:cardId',
            name: 'waitForOtherGuesses',
            component: WaitForOtherGuesses
        },
        {
            path: '/roundSummary/player/:playerId/game/:gameId/round/:roundId',
            name: 'roundSummary',
            component: RoundSummary
        },
        {
            path: '/game/:gameId/endOfGame',
            name: 'endOfGame',
            component: EndOfGame

        }
    ]
})

new Vue({
    render: h => h(App), router
}).$mount('#app');
