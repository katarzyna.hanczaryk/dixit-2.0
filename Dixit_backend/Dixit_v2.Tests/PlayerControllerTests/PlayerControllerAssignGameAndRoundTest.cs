﻿using Dixit_v2.Controllers;
using Dixit_v2.Model;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.PlayerControllerTests
{
    public class PlayerControllerAssignGameAndRoundTest : IntegrationTests
    {
        private Util util = new Util();
        [Fact]
        public async Task CanAssignGameAndRound()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<AssignedGameAndRound> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var playerId = util.CreatePlayer();
                result = await sut.AssignGameAndRoundAsync(playerId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var assignedGameAndRound = Assert.IsType<AssignedGameAndRound>(okResult.Value);
            Assert.NotNull(assignedGameAndRound);
            Assert.False(assignedGameAndRound.RoundId == 0);
            Assert.False(assignedGameAndRound.GameId == 0);
        }

        [Fact]
        public async Task CanAssignPlayerToExistingGame()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<AssignedGameAndRound> result;
            int roundId, gameId, secondPlayerOrder;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreateGameAndRound(util.CreatePlayer());
                int secondPlayerId = util.CreatePlayer();
                result = await sut.AssignGameAndRoundAsync(secondPlayerId);
                secondPlayerOrder = await _context.GamePlayers
                                                  .Where(n => n.PlayerId == secondPlayerId)
                                                  .Select(m => m.Order)
                                                  .FirstAsync();
                roundId = util.RetrieveValidRoundId();
                gameId = util.RetrieveValidGameId();
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var assignedGameAndRound = Assert.IsType<AssignedGameAndRound>(okResult.Value);
            Assert.NotNull(assignedGameAndRound);
            Assert.Equal(roundId, assignedGameAndRound.RoundId);
            Assert.Equal(gameId, assignedGameAndRound.GameId);
            Assert.Equal(2, secondPlayerOrder);
        }

        [Fact]
        public async Task CannotAssignGameAndRoundWhenIncorrectPlayerIdAsync()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<AssignedGameAndRound> actionResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                actionResult = await sut.AssignGameAndRoundAsync(0);
                scope.Dispose();
            }
            var result = Assert.IsType<ObjectResult>(actionResult.Result);
            var resultValue = Assert.IsType<String>(result.Value);
            Assert.Equal("Player has not been assigned to any game and round", resultValue);
            Assert.Equal(500, result.StatusCode);
        }

        [Fact]
        public async Task CannotAssignPlayerToFullGame()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<AssignedGameAndRound> actionResult;
            int fullGameId, fullRoundId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                int fifthPlayerAddedId = util.CreatePlayer();
                fullGameId = util.RetrieveValidGameId();
                fullRoundId = await _context.GameRounds
                                                .Where(m => m.GameId == fullGameId)
                                                .Select(n => n.RoundId)
                                                .FirstAsync();
                actionResult = await sut.AssignGameAndRoundAsync(fifthPlayerAddedId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var assignedGameAndRound = Assert.IsType<AssignedGameAndRound>(okResult.Value);
            Assert.NotNull(assignedGameAndRound);
            Assert.NotEqual(assignedGameAndRound.RoundId, fullRoundId);
            Assert.NotEqual(assignedGameAndRound.GameId, fullGameId);
        }
    }
}
