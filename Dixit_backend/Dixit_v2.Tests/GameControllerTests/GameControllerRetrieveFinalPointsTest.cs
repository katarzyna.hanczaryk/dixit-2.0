﻿using Dixit_v2.Controllers;
using Dixit_v2.Model;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerRetrieveFinalPointsTest : IntegrationTests
    {
        private Util util = new();

        [Fact]
        public void CanRetrievePointsForValidGameId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<FinalPoints>> result;
            var expectedResult = new List<FinalPoints>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFirstRoundWithOnePlayerGainMaxPointsPossible();
                int roundId = util.RetrieveValidRoundId();
                int storytellerId = util.RetrieveStorytellerId(util.RetrieveValidRoundId());
                var nonStorytellerIdList = util.RetrievePlayerIdsWithoutStoryteller(roundId);
                var playerNumber = util.RetrieveRequiredPlayerNumber();

                expectedResult.Add(new FinalPoints()
                {
                    PlayerName = util.RetrievePlayerName(storytellerId),
                    GamePoints = util.RetrieveBaseMaxPoints()
                });

                expectedResult.Add(new FinalPoints()
                {
                    PlayerName = util.RetrievePlayerName(nonStorytellerIdList[0]),
                    GamePoints = util.RetrieveBaseMaxPoints() + (playerNumber - 2) * util.RetrieveExtraSinglePoints()
                });

                expectedResult.Add(new FinalPoints()
                {
                    PlayerName = util.RetrievePlayerName(nonStorytellerIdList[1]),
                    GamePoints = util.RetrieveBaseMinimumPoints()
                });

                expectedResult.Add(new FinalPoints()
                {
                    PlayerName = util.RetrievePlayerName(nonStorytellerIdList[2]),
                    GamePoints = util.RetrieveBaseMinimumPoints()
                });

                int gameId = util.RetrieveValidGameId();
                result = sut.RetrieveFinalPoints(gameId);

                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<FinalPoints>>(okResult.Value).OrderBy(m => m.PlayerName);
            var orderedExpectedResult = expectedResult.OrderBy(m => m.PlayerName);
            Assert.NotNull(actualResult);
            Assert.Equal(orderedExpectedResult, actualResult);
        }

        [Fact]
        public void CannotRetrievePointsForInvalidGameId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<FinalPoints>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int invalidGameId = util.GenerateInvalidGameId();
                result = sut.RetrieveFinalPoints(invalidGameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrievePointsWhenGameIsNotOver()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<FinalPoints>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithOneGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                result = sut.RetrieveFinalPoints(gameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
