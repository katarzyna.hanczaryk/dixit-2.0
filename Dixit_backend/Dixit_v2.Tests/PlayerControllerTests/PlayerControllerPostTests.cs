﻿using AutoFixture;
using Dixit_v2.Controllers;
using Dixit_v2.Model;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.PlayerControllerTests
{
    public class PlayerControllerPostTests : IntegrationTests
    {
        [Fact]
        public async Task CanCreatePlayer()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                result = await sut.CreatePlayerAsync(CreatePlayerName());
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var playerId = Assert.IsType<int>(okResult.Value);
            Assert.False(playerId == 0);
        }

        [Fact]
        public async Task CanCreatePlayersCheckIds()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            ActionResult<int> result2;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                result = await sut.CreatePlayerAsync(CreatePlayerName());
                result2 = await sut.CreatePlayerAsync(CreatePlayerName());
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var okResult2 = Assert.IsType<OkObjectResult>(result2.Result);
            var playerId = Assert.IsType<int>(okResult.Value);
            var playerId2 = Assert.IsType<int>(okResult2.Value);
            Assert.Equal(playerId + 1, playerId2);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task CannotCreatePlayerWhenNullName(string name)
        {
            var fixture = new Fixture();
            fixture.Customize<PlayerName>(c => c.With(x => x.Name, name));
            var playerNameMock = fixture.Create<PlayerName>();
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            var actionResult = await sut.CreatePlayerAsync(playerNameMock);
            var result = Assert.IsType<ObjectResult>(actionResult.Result);
            Assert.Equal(500, result.StatusCode);
        }

        private static PlayerName CreatePlayerName()
        {
            var fixture = new Fixture();
            return fixture.Create<PlayerName>();
        }
    }
}
