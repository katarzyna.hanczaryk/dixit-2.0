﻿namespace Dixit_v2.Model
{
    public class AddGuessParams
    {
        public int PlayerId { get; set; }
        public int RoundId { get; set; }
        public int CardId { get; set; }

    }
}
