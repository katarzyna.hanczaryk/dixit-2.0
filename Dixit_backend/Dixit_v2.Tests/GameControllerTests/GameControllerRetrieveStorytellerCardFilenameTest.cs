﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerRetrieveStorytellerCardFilenameTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public async Task CanGetStorytellerCardFilenameForValidRoundId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<String> result;
            string expectedFilename;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int roundId = util.RetrieveValidRoundId();
                var storytellerRoundchosenCardId = util.RetrieveStorytellerRoundChosenCard(roundId);
                expectedFilename = _context.Cards
                                         .Where(m => m.Id == storytellerRoundchosenCardId)
                                         .Select(n => n.FileName)
                                         .FirstOrDefault();
                result = await sut.RetrieveStorytellerCardFilename(roundId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualFilename = Assert.IsType<string>(okResult.Value);
            Assert.NotNull(actualFilename);
            Assert.Equal(expectedFilename, actualFilename);
        }

        [Fact]
        public async Task CannotGetStorytellerCardFilenameForInvalidRoundId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<String> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int invalidRoundId = util.GenerateInvalidRoundId();
                result = await sut.RetrieveStorytellerCardFilename(invalidRoundId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public async Task CannotGetStorytellerCardFilenameWhenStorytellerHasNotSelectedAnyCardYet()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<String> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                int roundId = util.RetrieveValidRoundId();
                result = await sut.RetrieveStorytellerCardFilename(roundId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
