﻿using Microsoft.EntityFrameworkCore;

namespace Dixit_v2.Model
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public DbSet<Player> Players { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<CardInGame> CardsInGame { get; set; }
        public DbSet<Round> Rounds { get; set; }
        public DbSet<GamePlayer> GamePlayers { get; set; }
        public DbSet<RoundChosenCard> RoundChosenCards { get; set; }
        public DbSet<CardDescription> CardDescriptions { get; set; }
        public DbSet<GameRound> GameRounds { get; set; }
        public DbSet<PlayerGuessCard> PlayerGuessCards { get; set; }
        public DbSet<DataForPointsCalculation> vwDataForPointsCalculation { get; set; }
        public DbSet<Game> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DataForPointsCalculation>(entity =>
            {
                entity.HasNoKey();
            });
        }
    }
}
