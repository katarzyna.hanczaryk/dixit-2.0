﻿using Dixit_v2.Common;
using Dixit_v2.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dixit_v2.Controllers
{
    public class GameController : BaseApiController<GameController>
    {
        public GameController(ApplicationDbContext db, ILoggerFactory loggerFactory, IConfiguration iconfig) : base(db, loggerFactory, iconfig) { }

        [HttpGet("api/game/{gameId}")]
        public async Task<ActionResult<int>> GetGamePlayerNumber(int gameId)
        {
            try
            {
                int playerNumber = await RetrievePlayerNumberAsync(gameId);
                if (playerNumber > 0)
                    return Ok(playerNumber);
                else
                    throw new Exception("Incorrect number of players was retrieved.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Retrieving number of players for a given gameId failed");
                return StatusCode(500, "Retrieval of number of players of the game failed.");
            }
        }

        [HttpGet("api/round/{roundId}/roundCardsSelected")]
        public async Task<ActionResult<bool>> AreAllCardsChosenForRoundAsync(int roundId)
        {
            try
            {
                var roundChosenCards = await AreAllCardsPerRoundChosenAsync(roundId);
                return Ok(roundChosenCards);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "The info whether all players selected cards in this round have not been retrieved.");
                return StatusCode(500, "The info whether all players selected cards in this round have not been retrieved.");
            }
        }

        [HttpGet("api/round/{roundId}/selectedCards")]
        public ActionResult<IEnumerable<Card>> RetrieveAllRoundSelectedCards(int roundId)
        {
            try
            {
                var result = RetrieveRoundSelectedCards(roundId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"All selected cards for round {roundId} hasn't been retrieved");
                return StatusCode(500, "The selected cards for the given round couldn't been retrieved");
            }
        }

        [HttpGet("api/game/{gameId}/points")]
        public ActionResult<IEnumerable<PlayersPoints>> GetPlayersRoundAndGamePoints(int gameId)
        {
            try
            {
                var result = RetrievePlayersRoundAndGamePoints(gameId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Player points for game {gameId} haven't been retrieved");
                return StatusCode(500, "Points haven't been retrieved from the db.");
            }
        }

        [HttpGet("api/round/{roundId}/guessesDone")]
        public async Task<ActionResult<bool>> AreAllGuessesDone(int roundId)
        {
            try
            {
                var result = await AllRoundGuessesDone(roundId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Data whether all guesses have been done for round {roundId} has not been retrieved.");
                return StatusCode(500, "Data whether all round guesses haven't been retrieved from the db.");
            }
        }

        [HttpGet("api/round/{roundId}/storytellerCard")]
        public async Task<ActionResult<string>> RetrieveStorytellerCardFilename(int roundId)
        {
            try
            {
                var result = (await GetStorytellerCardAsync(roundId)).FileName;
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Storyteller card filename for {roundId} has not been retrieved.");
                return StatusCode(500, "Storyteller card filename has not been retrieved.");
            }
        }

        [HttpGet("api/game/{gameId}/isGameOver")]
        public ActionResult<bool> IsGameOver(int gameId)
        {
            try
            {
                bool result = IsGameCompleted(gameId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"The info whether the game is over has not been retrieved for game {gameId}.");
                return StatusCode(500, "The info whether the game is over has not been retrieved.");
            }
        }

        [HttpGet("api/game/{gameId}/finalPoints")]
        public ActionResult<IEnumerable<FinalPoints>> RetrieveFinalPoints(int gameId)
        {
            try
            {
                var result = GetFinalPoints(gameId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Final points for the game {gameId} has not been retrieved.");
                return StatusCode(500, "Final game points haven't been retrieved.");
            }
        }

        [HttpGet("api/game/{gameId}/winners")]
        public ActionResult<IEnumerable<string>> RetrieveWinnerNames(int gameId)
        {
            try
            {
                var result = GetWinnerPlayerNames(gameId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Winner list for the game {gameId} has not been retrieved.");
                return StatusCode(500, "Winner list hasn't been retrieved.");
            }
        }

        [HttpGet("api/game/{gameId}/player/{playerId}/newRound")]
        public async Task<ActionResult<int>> PrepareNewRound(int gameId, int playerId)
        {
            try
            {
                int currentRound = await AddNewRoundAndChangeStorytellerAsync(playerId, gameId);
                return Ok(currentRound);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "The new round hasn't been created.");
                return StatusCode(500, "The missing card hasn't been assigned.");
            }
        }
    }
}
