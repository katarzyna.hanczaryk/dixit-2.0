﻿using Dixit_v2.Common;
using Dixit_v2.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dixit_v2.Controllers
{
    public class CardController : BaseApiController<CardController>
    {
        public CardController(ApplicationDbContext db, ILoggerFactory loggerFactory, IConfiguration iconfig) : base(db, loggerFactory, iconfig) { }

        [HttpGet("api/card/player/{playerId}/game/{gameId}/generateCards")]
        public async Task<ActionResult> GeneratePlayerCardsAsync(int playerId, int gameId)
        {
            try
            {
                await GenerateRandomPlayerCardsAsync(playerId, gameId);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Cards haven't been generated for player {playerId}");
                return StatusCode(500, "Retrieval of cards for player failed");
            }
        }

        [HttpGet("api/card/player/{playerId}/game/{gameId}")]
        public ActionResult<IEnumerable<Card>> RetrievePlayerRoundCards(int playerId, int gameId)
        {
            try
            {
                var cards = RetrieveAllPlayerCards(playerId, gameId);
                if (cards.Any())
                    return Ok(cards);
                else
                    throw new Exception("Cards haven't been retrieved from the db");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Cards haven't been retrieved for player {playerId}");
                return StatusCode(500, "Retrieval of cards for player failed");
            }
        }

        [HttpPost("api/card/chosenCard")]
        public async Task<ActionResult> AddRoundSelectedCardAsync([FromBody] SelectedCard selectedCard)
        {
            try
            {
                await AddRoundChosenCardAsync(selectedCard.ChosenCardId, selectedCard.RoundId);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"The chosen card {selectedCard.ChosenCardId} for round {selectedCard.RoundId}" +
                    $"has not been added");
                return StatusCode(500, "The chosen card hasn't been added.");
            }
        }

        [HttpGet("api/card/chosenCard/{chosenCardId}")]
        public async Task<ActionResult<string>> GetPlayerChosenCardAsync(int chosenCardId)
        {
            try
            {
                var result = await RetrievePlayerChosenCardAsync(chosenCardId);
                if (result != null)
                    return Ok(result);
                else
                    throw new Exception("No data retrieved from db.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"The card for id {chosenCardId} hasn't been retrieved.");
                return StatusCode(500, "The selected card hasn't been retrieved.");
            }
        }

        [HttpGet("api/card/player/{playerId}/remainingCards")]
        public ActionResult<IEnumerable<string>> RetrievePlayerRemainingCardAddresses(int playerId)
        {
            try
            {
                var result = RetrieveRemainingCardAddresses(playerId);
                if (result.Any())
                    return Ok(result);
                throw new Exception("None cards have been retrieved from db.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"The remaining cards for player {playerId} haven't been retrieved.");
                return StatusCode(500, "The remaining cards haven't been retrieved.");
            }
        }

        [HttpPost("api/card/round/{roundId}/cardDescription")]
        public async Task<ActionResult> AddDescriptionAsync([FromBody] Description description)
        {
            try
            {
                await AddCardDescriptionAsync(description.RoundId, description.Hint);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "The description has not been added.");
                return StatusCode(500, "The description has not been added.");
            }
        }

        [HttpGet("api/card/round/{roundId}/hint")]
        public async Task<ActionResult<string>> GetStorytellerHintAsync(int roundId)
        {
            try
            {
                var result = await RetrieveHintAsync(roundId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"The storyteller hint for round {roundId} has not been returned");
                return StatusCode(500, "The hint has not been returned");
            }
        }

        [HttpPost("api/card/{cardId}/player/{playerId}/round/{roundId}/guess")]
        public async Task<ActionResult> AddGuessCardAsync([FromBody] AddGuessParams addGuessParams)
        {
            try
            {
                await AddPlayerGuessCardAsync(addGuessParams.PlayerId, addGuessParams.RoundId,
                   addGuessParams.CardId);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"The guess for a player {addGuessParams.PlayerId} hasn't been added");
                return StatusCode(500, "The guess hasn't been saved.");
            }
        }

        [HttpGet("api/game/{gameId}/player/{playerId}/missingCard")]
        public async Task<ActionResult> AssignMissingPlayerCard(int playerId, int gameId)
        {
            try
            {
                await AssignPlayerMissingCardsAsync(playerId, gameId);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"The card hasn't been assigned to player {playerId}.");
                return StatusCode(500, "The missing card hasn't been assigned.");
            }
        }
    }
}
