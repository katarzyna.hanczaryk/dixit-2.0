﻿using AutoFixture;
using Dixit_v2.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dixit_v2.Tests.Common
{
    public class Util : IntegrationTests
    {
        private int currentCardId = 1;
        public int CreatePlayer()
        {
            var fixture = new Fixture();
            var player = fixture.Build<Player>()
                                .Without(p => p.Id)
                                .Create();
            _context.Players.Add(player);
            _context.SaveChanges();
            return _context.Players
                          .OrderBy(n => n.Id)
                          .Select(m => m.Id)
                          .Last();
        }

        public void CreateGameAndRound(int playerId)
        {
            _context.Database.ExecuteSqlInterpolated(@$"INSERT INTO [dbo].[Games] DEFAULT VALUES 
                                    INSERT INTO [dbo].[Rounds] ([StorytellerId]) VALUES ({ playerId })");
            var roundId = _context.Rounds
                                 .OrderBy(m => m.Id)
                                 .Select(n => n.Id)
                                 .Last();
            var gameId = _context.Games
                                .OrderBy(m => m.Id)
                                .Select(n => n.Id)
                                .Last();
            var gameRound = new GameRound()
            {
                GameId = gameId,
                RoundId = roundId
            };
            _context.GameRounds.Add(gameRound);
            var gamePlayer = new GamePlayer()
            {
                GameId = gameId,
                PlayerId = playerId,
                Order = 1
            };
            _context.GamePlayers
                   .Add(gamePlayer);
            _context.SaveChanges();
        }

        public void CreateRound(int storytellerId)
        {
            _context.Database.ExecuteSqlInterpolated($"INSERT INTO [dbo].[Rounds] ([StorytellerId]) VALUES ({storytellerId})");
            var roundId = _context.Rounds
                                 .OrderBy(m => m.Id)
                                 .Select(n => n.Id)
                                 .Last();
            var gameId = _context.Games
                                .OrderBy(m => m.Id)
                                .Select(n => n.Id)
                                .Last();
            var gameRound = new GameRound()
            {
                GameId = gameId,
                RoundId = roundId
            };
            _context.GameRounds.Add(gameRound);
            _context.SaveChanges();
        }

        public void AssignPlayerToExistingGame(int playerId)
        {
            var gameId = _context.Games
                                .OrderBy(m => m.Id)
                                .Select(n => n.Id)
                                .Last();
            var lastOrder = _context.GamePlayers
                                   .Where(m => m.GameId == gameId)
                                   .OrderBy(n => n.Id)
                                   .Select(k => k.Order)
                                   .LastOrDefault();
            var newGamePlayer = new GamePlayer()
            {
                GameId = gameId,
                PlayerId = playerId,
                Order = (byte)(lastOrder + 1)
            };
            _context.GamePlayers
                   .Add(newGamePlayer);
            _context.SaveChanges();
        }

        public int GenerateNonStorytellerCardGuess(int playerId, int roundId)
        {
            var roundSelectedCardIds = RetrieveRoundSelectedCardIds(roundId);
            var storytellerRoundChosenCard = RetrieveStorytellerRoundChosenCard(roundId);
            var storytellerSelectedCardId = _context.RoundChosenCards
                                                    .Where(m => m.Id == storytellerRoundChosenCard)
                                                    .Select(n => n.CardId)
                                                    .FirstOrDefault();
            var playerSelectedCardId = RetrievePlayerSelectedCard(playerId, roundId);
            return roundSelectedCardIds.Where(m => m != storytellerRoundChosenCard && m != playerSelectedCardId)
                                       .FirstOrDefault();
        }

        public IEnumerable<int> RetrieveRoundSelectedCardIds(int roundId)
        {
            return _context.RoundChosenCards
                           .Where(m => m.RoundId == roundId)
                           .Select(n => n.CardId);
        }

        public int RetrievePlayerSelectedCard(int playerId, int roundId)
        {
            var roundSelectedCardId = _context.RoundChosenCards
                                              .Where(m => m.RoundId == roundId)
                                              .Select(n => n.CardId);
            var gamePlayersCardsId = _context.CardsInGame
                                             .Where(m => m.PlayerId == playerId)
                                             .Select(n => n.CardId);
            return roundSelectedCardId.Intersect(gamePlayersCardsId)
                                      .FirstOrDefault();
        }

        public int RetrieveValidRoundId()
        {
            return _context.Rounds
                          .OrderBy(m => m.Id)
                          .Select(n => n.Id)
                          .Last();
        }

        public int RetrieveValidGameId()
        {
            return _context.Games
                          .OrderBy(m => m.Id)
                          .Select(n => n.Id)
                          .Last();
        }
        public string RetrieveCardFilename(int cardId)
        {
            return _context.Cards
                           .Where(m => m.Id == cardId)
                           .Select(n => n.FileName)
                           .FirstOrDefault();
        }

        public int GenerateInvalidPlayerId()
        {
            return _context.Players
                          .OrderBy(m => m.Id)
                          .Select(n => n.Id)
                          .Last() + 1;
        }

        public int GenerateInvalidRoundId()
        {
            return _context.Rounds
                          .OrderBy(m => m.Id)
                          .Select(n => n.Id)
                          .Last() + 1;
        }

        public int RetrieveRequiredPlayerNumber()
        {
            return Int32.Parse(_iConfig.GetSection("GameRules")
                        .GetSection("RequiredPlayersNumber")
                        .Value);
        }

        public int RetrieveTotalCardsNumber()
        {
            return Int32.Parse(_iConfig.GetSection("GameRules")
                        .GetSection("TotalCards")
                        .Value);
        }

        public int RetrieveBaseMaxPoints()
        {
            return Int32.Parse(_iConfig.GetSection("GameRules")
                         .GetSection("BaseMaxPoints")
                         .Value);
        }

        public int RetrieveExtraSinglePoints()
        {
            return Int32.Parse(_iConfig.GetSection("GameRules")
                         .GetSection("ExtraSinglePoints")
                         .Value);
        }
        public int RetrieveStorytellerId(int roundId)
        {
            return _context.Rounds
                           .Where(n => n.Id == roundId)
                           .OrderBy(k => k.Id)
                           .Select(m => m.StorytellerId)
                           .Last();
        }

        public int RetrieveNonStorytellerPlayerId()
        {
            int storytellerId = RetrieveStorytellerId(RetrieveValidRoundId());
            return _context.Players
                           .Where(m => m.Id != storytellerId)
                           .Select(n => n.Id)
                           .First();
        }

        public string RetrieveHint(int cardDescriptionId)
        {
            return _context.CardDescriptions
                           .Where(m => m.Id == cardDescriptionId)
                           .Select(n => n.Description)
                           .First();
        }

        public string RetrievePlayerName(int playerId)
        {
            return _context.Players
                           .Where(m => m.Id == playerId)
                           .Select(n => n.Name)
                           .FirstOrDefault();
        }

        public int RetrieveBaseMediumPoints()
        {
            return Int32.Parse(_iConfig.GetSection("GameRules")
                        .GetSection("BaseMediumPoints")
                        .Value);
        }

        public int RetrieveBaseMinimumPoints()
        {
            return Int32.Parse(_iConfig.GetSection("GameRules")
                        .GetSection("BaseMinPoints")
                        .Value);
        }

        public int RetrieveWinningNumberPoints()
        {
            return Int32.Parse(_iConfig.GetSection("GameRules")
                        .GetSection("WinningPoints")
                        .Value);
        }

        public int GenerateInvalidCardId()
        {
            return _context.Cards
                           .OrderBy(m => m.Id)
                           .Select(n => n.Id)
                           .Last() + 1;
        }

        public int GenerateInvalidGameId()
        {
            return _context.Games
                           .OrderBy(n => n.Id)
                           .Select(m => m.Id)
                           .Last() + 1;
        }

        public int RetrievePlayerGuessCard(int playerId)
        {
            int roundId = RetrieveValidRoundId();
            var playersCardsInGame = _context.CardsInGame
                                             .Where(m => m.PlayerId == playerId)
                                             .Select(n => n.CardId);
            var roundChosenCards = _context.RoundChosenCards
                                           .Where(m => m.RoundId == roundId)
                                           .Select(n => n.CardId);
            return roundChosenCards.Except(playersCardsInGame)
                                   .FirstOrDefault();
        }

        public void CreatePlayerGameAndRound()
        {
            int playerId = CreatePlayer();
            CreateGameAndRound(playerId);
        }

        public void AssignCardsToPlayer(int playerId)
        {
            int totalCards = RetrieveTotalCardsNumber();
            var cardsToBeAssignedList = new List<CardInGame>();
            while (cardsToBeAssignedList.Count < totalCards)
            {
                cardsToBeAssignedList.Add(new CardInGame()
                {
                    PlayerId = playerId,
                    CardId = this.currentCardId++
                });
            }
            _context.CardsInGame
                    .AddRange(cardsToBeAssignedList);
            _context.SaveChanges();
        }

        public void SelectCard(int playerId, int roundId)
        {
            int cardId = _context.CardsInGame
                                 .Where(m => m.PlayerId == playerId)
                                 .Select(n => n.CardId)
                                 .First();
            _context.Database.ExecuteSqlInterpolated(@$"INSERT INTO [dbo].[RoundChosenCards]
                                    (RoundId, CardId) VALUES ({ roundId }, { cardId })");
            _context.SaveChanges();
        }

        public int RetrievePlayerUnusedCardId(int playerId)
        {
            var roundChosenCards = _context.RoundChosenCards
                                           .Select(m => m.CardId);
            return _context.CardsInGame
                           .Where(m => m.PlayerId == playerId)
                           .Select(n => n.CardId)
                           .Except(roundChosenCards)
                           .FirstOrDefault();
        }

        public List<Card> RetrievePlayerCards(int playerId)
        {
            var playerCardIds = _context.CardsInGame
                                      .Where(m => m.PlayerId == playerId)
                                      .Select(m => m.CardId);
            return _context.Cards
                           .Where(m => playerCardIds.Contains(m.Id))
                           .ToList();
        }

        public List<Card> RetrieveAllPlayerUnusedCards(int playerId)
        {
            var allPlayersCards = RetrievePlayerCards(playerId);
            var roundChosenCardIds = _context.RoundChosenCards
                                           .Select(m => m.CardId);
            var roundChosenCards = _context.Cards
                                           .Where(m => roundChosenCardIds.Contains(m.Id));
            return allPlayersCards.Except(roundChosenCards)
                                  .ToList();
        }

        public void AddGuess(int playerId, int roundId, int cardId)
        {
            _context.Add(new PlayerGuessCard()
            {
                PlayerId = playerId,
                RoundChosenCardId = _context.RoundChosenCards
                                            .Where(m => m.CardId == cardId
                                                         && m.RoundId == roundId)
                                            .First().Id
            });
            _context.SaveChanges();
        }

        public int RetrieveStorytellerRoundChosenCard(int roundId)
        {
            int storytellerId = RetrieveStorytellerId(roundId);
            var storytellerCardsInGame = _context.CardsInGame
                                                 .Where(m => m.PlayerId == storytellerId)
                                                 .Select(n => n.CardId);
            var roundChosenCards = _context.RoundChosenCards
                                           .Where(m => m.RoundId == roundId)
                                           .Select(n => n.CardId);
            return storytellerCardsInGame.Intersect(roundChosenCards).FirstOrDefault();
        }

        public int AddCardDescription(int roundId)
        {
            var fixture = new Fixture();
            var cardDescription = fixture.Build<CardDescription>()
                                         .Without(p => p.Id)
                                         .With(r => r.RoundId, roundId)
                                         .Create();
            _context.CardDescriptions.Add(cardDescription);
            _context.SaveChanges();
            return _context.CardDescriptions
                           .Select(m => m.Id)
                           .FirstOrDefault();
        }

        public List<int> RetrieveAllPlayersId()
        {
            return _context.Players
                           .OrderBy(n => n.Id)
                           .Select(m => m.Id)
                           .ToList();
        }

        public List<int> RetrievePlayerIdsWithoutStoryteller(int roundId)
        {
            int storytellerId = RetrieveStorytellerId(roundId);
            return RetrieveAllPlayersId().Where(n => n != storytellerId)
                                         .OrderBy(m => m)
                                         .ToList();
        }

        public void MockFullGameBeforeCardsGeneration()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            playerIdList.Where(m => m != storytellerId)
                        .ToList()
                        .ForEach(n => AssignPlayerToExistingGame(n));
        }

        public void MockFullGameBeforeCardsSelection()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            playerIdList.Where(m => m != storytellerId)
                        .ToList()
                        .ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
        }

        public void MockFullGameWithAllRoundChosenCards()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            playerIdList.Where(m => m != storytellerId)
                        .ToList()
                        .ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            playerIdList.ForEach(n => SelectCard(n, roundId));
        }

        public void MockCompleteGameWithNotAllRoundChosenCards()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            playerIdList.Where(m => m != storytellerId)
                        .ToList()
                        .ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            for (int i = 0; i < requiredPlayerNumber - 1; i++)
            {
                SelectCard(playerIdList.ElementAt(i), roundId);
            }
        }

        public void MockFullGameWithAllGuessedStorytellerCard()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            var nonStorytellerPlayersList = playerIdList.Where(m => m != storytellerId).ToList();
            nonStorytellerPlayersList.ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            playerIdList.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            nonStorytellerPlayersList.ForEach(n => AddGuess(n, roundId, storytellerRoundCardId));
        }

        public void MockFullGameWithNoneGuessedStorytellerCard()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            var nonStorytellerPlayersList = playerIdList.Where(m => m != storytellerId).ToList();
            nonStorytellerPlayersList.ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            playerIdList.ForEach(n => SelectCard(n, roundId));
            foreach (int playerId in nonStorytellerPlayersList)
            {
                var guessCardId = GenerateNonStorytellerCardGuess(playerId, roundId);
                AddGuess(playerId, roundId, guessCardId);
            }
        }

        public void MockFullGameWithOneGuessedStorytellerCard()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            var nonStorytellerPlayersList = playerIdList.Where(m => m != storytellerId)
                                                        .OrderBy(n => n)
                                                        .ToList();
            nonStorytellerPlayersList.ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            playerIdList.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            int currentGuessCardId = storytellerRoundCardId;
            foreach (int playerId in nonStorytellerPlayersList)
            {
                AddGuess(playerId, roundId, currentGuessCardId);
                currentGuessCardId = RetrievePlayerSelectedCard(playerId, roundId);
            }
        }

        public void MockFullGameWithTwoGuessedStorytellerCards()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            var nonStorytellerPlayersList = playerIdList.Where(m => m != storytellerId)
                                                        .OrderBy(n => n)
                                                        .ToList();
            nonStorytellerPlayersList.ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            playerIdList.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            AddGuess(nonStorytellerPlayersList[0], roundId, storytellerRoundCardId);
            int currentGuessCardId = storytellerRoundCardId;
            for (int i = 1; i < nonStorytellerPlayersList.Count; i++)
            {
                int currentPlayerId = nonStorytellerPlayersList[i];
                AddGuess(currentPlayerId, roundId, currentGuessCardId);
                currentGuessCardId = RetrievePlayerSelectedCard(currentPlayerId, roundId);
            }
        }

        public void MockSecondRoundWhereEveryPersonGuessed()
        {
            var gamePlayerIds = RetrieveAllPlayersId().ToList();
            int secondStorytellerId = gamePlayerIds[1];
            CreateRound(secondStorytellerId);
            int roundId = RetrieveValidRoundId();
            gamePlayerIds.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            var nonStorytellerPlayersList = RetrievePlayerIdsWithoutStoryteller(roundId).ToList();
            nonStorytellerPlayersList.ForEach(n => AddGuess(n, roundId, storytellerRoundCardId));
        }

        public void MockSecondRoundWhereNobodyGuessed()
        {
            var gamePlayerIds = RetrieveAllPlayersId().ToList();
            int secondStorytellerId = gamePlayerIds[1];
            CreateRound(secondStorytellerId);
            int roundId = RetrieveValidRoundId();
            gamePlayerIds.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            var nonStorytellerPlayersList = RetrievePlayerIdsWithoutStoryteller(roundId).ToList();
            foreach (int playerId in nonStorytellerPlayersList)
            {
                var guessCardId = GenerateNonStorytellerCardGuess(playerId, roundId);
                AddGuess(playerId, roundId, guessCardId);
            }
        }

        // player1 -> guessed storyteller; the remaining players -> guessed player1 card
        public void MockSecondRoundWhereOnePlayerGuessedStorytellerCard()
        {
            var gamePlayerIds = RetrieveAllPlayersId().ToList();
            int secondStorytellerId = gamePlayerIds[1];
            CreateRound(secondStorytellerId);
            int roundId = RetrieveValidRoundId();
            gamePlayerIds.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            var nonStorytellerPlayersList = RetrievePlayerIdsWithoutStoryteller(roundId);
            int winnerPlayerId = nonStorytellerPlayersList[0];
            AddGuess(winnerPlayerId, roundId, storytellerRoundCardId);
            int winnerPlayerRoundSelectedCard = RetrievePlayerSelectedCard(winnerPlayerId, roundId);
            for (int i = 1; i < nonStorytellerPlayersList.Count; i++)
                AddGuess(nonStorytellerPlayersList[i], roundId, winnerPlayerRoundSelectedCard);
        }

        // player1,player2 -> guessed storyteller; the remaining players -> guessed player1 card
        public void MockSecondRoundWhereTwoPlayersGuessedStorytellerCard()
        {
            var gamePlayerIds = RetrieveAllPlayersId().ToList();
            int secondStorytellerId = gamePlayerIds[1];
            CreateRound(secondStorytellerId);
            int roundId = RetrieveValidRoundId();
            gamePlayerIds.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            var nonStorytellerPlayersList = RetrievePlayerIdsWithoutStoryteller(roundId);
            int winnerPlayerId1 = nonStorytellerPlayersList[0];
            int winnerPlayerId2 = nonStorytellerPlayersList[1];
            AddGuess(winnerPlayerId1, roundId, storytellerRoundCardId);
            AddGuess(winnerPlayerId2, roundId, storytellerRoundCardId);
            int winnerPlayerRoundSelectedCard = RetrievePlayerSelectedCard(winnerPlayerId1, roundId);
            for (int i = 2; i < nonStorytellerPlayersList.Count; i++)
                AddGuess(nonStorytellerPlayersList[i], roundId, winnerPlayerRoundSelectedCard);
        }

        public void MockFullGameWithNotAllGueesesDone()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            var nonStorytellerPlayersList = playerIdList.Where(m => m != storytellerId).ToList();
            nonStorytellerPlayersList.ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            playerIdList.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            for (int i = 1; i < nonStorytellerPlayersList.Count; i++)
            {
                AddGuess(nonStorytellerPlayersList[i], roundId, storytellerRoundCardId);
            }
        }

        public int MockFirstRoundWithOnePlayerGainMaxPointsPossible()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            var nonStorytellerPlayersList = playerIdList.Where(m => m != storytellerId)
                                                        .OrderBy(n => n)
                                                        .ToList();
            nonStorytellerPlayersList.ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            playerIdList.ForEach(n => SelectCard(n, roundId));

            int playerWinnerId = nonStorytellerPlayersList[0];
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            AddGuess(playerWinnerId, roundId, storytellerRoundCardId);
            int winnerRoundChosenCard = RetrievePlayerSelectedCard(playerWinnerId, roundId);
            for (int i = 1; i < nonStorytellerPlayersList.Count; i++)
                AddGuess(nonStorytellerPlayersList[i], roundId, winnerRoundChosenCard);
            return playerWinnerId;
        }

        public void MockSubsequentRoundsWinnerPlayerGainsMaxPoints(int winnerPlayerId)
        {
            int currentStorytellerId = RetrieveNextRoundStorytellerId();
            var gamePlayerIds = RetrieveAllPlayersId().ToList();
            CreateRound(currentStorytellerId);
            int roundId = RetrieveValidRoundId();
            gamePlayerIds.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            var nonStorytellerPlayerIds = gamePlayerIds.Where(m => m != currentStorytellerId)
                                                       .ToList();
            if (winnerPlayerId == currentStorytellerId)
            {
                int playerWhoGuessed = nonStorytellerPlayerIds[0];
                AddGuess(playerWhoGuessed, roundId, storytellerRoundCardId);
                int playerWhoGuessedRoundSelectedCard = RetrievePlayerSelectedCard(playerWhoGuessed, roundId);
                for (int i = 1; i < nonStorytellerPlayerIds.Count; i++)
                    AddGuess(nonStorytellerPlayerIds[i], roundId, playerWhoGuessedRoundSelectedCard);
            }
            else
            {
                AddGuess(winnerPlayerId, roundId, storytellerRoundCardId);
                var remainingNonStorytellerPlayers = nonStorytellerPlayerIds.Where(m => m != winnerPlayerId)
                                                                            .ToList();
                var winnerSelectedCard = RetrievePlayerSelectedCard(winnerPlayerId, roundId);
                remainingNonStorytellerPlayers.ForEach(n => AddGuess(n, roundId, winnerSelectedCard));
            }
        }

        public int RetrieveNextRoundStorytellerId()
        {
            int previousRoundId = RetrieveValidRoundId();
            int previousStorytellerId = RetrieveStorytellerId(previousRoundId);
            int previousStorytellerOrder = _context.GamePlayers
                                                   .Where(m => m.PlayerId == previousStorytellerId)
                                                   .Select(n => n.Order)
                                                   .FirstOrDefault();
            int totalPlayers = RetrieveRequiredPlayerNumber();
            int nextStorytellerOrder = totalPlayers == previousStorytellerOrder ? 1 : previousStorytellerOrder + 1;
            return _context.GamePlayers
                           .Where(m => m.Order == nextStorytellerOrder)
                           .Select(n => n.PlayerId)
                           .FirstOrDefault();
        }

        public int CalculateWinnerPlayerRoundPoints(int playerId, int roundId)
        {
            bool isStoryteller = RetrieveStorytellerId(roundId) == playerId;
            int playersNumber = RetrieveRequiredPlayerNumber();
            int nonStorytellerMaxPointsNumber = RetrieveBaseMaxPoints() + (playersNumber - 2) * RetrieveExtraSinglePoints();
            return isStoryteller ? RetrieveBaseMaxPoints() : nonStorytellerMaxPointsNumber;
        }

        public void MockFullGameWithWinner()
        {
            int maxPointsNumber = RetrieveWinningNumberPoints();
            int winnerPlayerId = MockFirstRoundWithOnePlayerGainMaxPointsPossible();
            int currentWinnerPlayerTotalPoints = CalculateWinnerPlayerRoundPoints(winnerPlayerId, RetrieveValidRoundId());
            while (currentWinnerPlayerTotalPoints < maxPointsNumber)
            {
                MockSubsequentRoundsWinnerPlayerGainsMaxPoints(winnerPlayerId);
                currentWinnerPlayerTotalPoints += CalculateWinnerPlayerRoundPoints(winnerPlayerId, RetrieveValidRoundId());
            }
        }

        public void MockSubsequentRoundWhenAllGuessed(int storytellerId)
        {
            CreateRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            var gamePlayerIds = RetrieveAllPlayersId().ToList();
            gamePlayerIds.ForEach(n => SelectCard(n, roundId));

            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            var nonStorytellerPlayersList = RetrievePlayerIdsWithoutStoryteller(roundId).ToList();
            nonStorytellerPlayersList.ForEach(n => AddGuess(n, roundId, storytellerRoundCardId));
        }

        public void MockFullGameWithOneGuessMissing()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            var nonStorytellerPlayersList = playerIdList.Where(m => m != storytellerId).ToList();
            nonStorytellerPlayersList.ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            playerIdList.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            for (int i = 1; i < nonStorytellerPlayersList.Count; i++)
                AddGuess(nonStorytellerPlayersList[i], roundId, storytellerRoundCardId);
        }

        public void MockFullGameWithMultipleGuessMissing()
        {
            int requiredPlayerNumber = RetrieveRequiredPlayerNumber();
            var playerIdList = new List<int>();
            for (int i = 0; i < requiredPlayerNumber; i++)
                playerIdList.Add(CreatePlayer());
            int storytellerId = playerIdList.First();
            CreateGameAndRound(storytellerId);
            int roundId = RetrieveValidRoundId();
            var nonStorytellerPlayersList = playerIdList.Where(m => m != storytellerId).ToList();
            nonStorytellerPlayersList.ForEach(n => AssignPlayerToExistingGame(n));
            playerIdList.ForEach(n => AssignCardsToPlayer(n));
            playerIdList.ForEach(n => SelectCard(n, roundId));
            int storytellerRoundCardId = RetrieveStorytellerRoundChosenCard(roundId);
            AddGuess(nonStorytellerPlayersList[0], roundId, storytellerRoundCardId);
        }
    }
}
