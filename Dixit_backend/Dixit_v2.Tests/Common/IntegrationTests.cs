﻿using Dixit_v2.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.IO;

namespace Dixit_v2.Tests.Common
{
    public abstract class IntegrationTests
    {
        protected readonly ILoggerFactory _loggerFactory;
        private const string CONNECTION_STRING = "Server=.;Database=Dixit_test;Trusted_Connection=True;MultipleActiveResultSets=True";
        protected readonly ApplicationDbContext _context;
        protected readonly IConfiguration _iConfig;
        protected int Seed = 1;

        protected IntegrationTests()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(CONNECTION_STRING);
            _context = new ApplicationDbContext(optionsBuilder.Options);
            _loggerFactory = new LoggerFactory();
            _iConfig = new ConfigurationBuilder()
                                .SetBasePath(Directory.GetCurrentDirectory())
                                .AddJsonFile(@"appsettings.json", false, false)
                                .AddEnvironmentVariables()
                                .Build();
        }
    }
}
