﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.CardControllerTests
{
    public class CardControllerRetrievePlayerRemainingCardAddressesTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public void CanRetrieveRemainingCardsForValidPlayerId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<string>> actualResult;
            var expectedRemainigCardAddresses = new List<string>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                expectedRemainigCardAddresses = util.RetrieveAllPlayerUnusedCards(playerId)
                                                    .Select(m => m.FileName)
                                                    .ToList();
                actualResult = sut.RetrievePlayerRemainingCardAddresses(playerId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(actualResult.Result);
            var actualRemainingCardAddresses = Assert.IsType<List<string>>(okResult.Value);
            Assert.NotNull(actualRemainingCardAddresses);
            Assert.Equal(expectedRemainigCardAddresses, actualRemainingCardAddresses);
        }

        [Fact]
        public void CannotRetrieveRemainingCardsForInvalidPlayerId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<string>> actualResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int invalidPlayerId = util.GenerateInvalidPlayerId();
                actualResult = sut.RetrievePlayerRemainingCardAddresses(invalidPlayerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(actualResult.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CanRetrieveRemainingCardsForValidNormalPlayer()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<string>> actualResult;
            var expectedRemainigCardAddresses = new List<string>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int normalPlayerId = util.RetrieveNonStorytellerPlayerId();
                expectedRemainigCardAddresses = util.RetrieveAllPlayerUnusedCards(normalPlayerId)
                                                    .Select(m => m.FileName)
                                                    .ToList();
                actualResult = sut.RetrievePlayerRemainingCardAddresses(normalPlayerId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(actualResult.Result);
            var actualRemainingCardAddresses = Assert.IsType<List<string>>(okResult.Value);
            Assert.NotNull(actualRemainingCardAddresses);
            Assert.Equal(expectedRemainigCardAddresses, actualRemainingCardAddresses);
        }

        [Fact]
        public void CanRetrieveRemainingCardsForValidStoryteller()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<string>> actualResult;
            var expectedRemainigCardAddresses = new List<string>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int roundId = util.RetrieveValidRoundId();
                int storytellerId = util.RetrieveStorytellerId(roundId);
                expectedRemainigCardAddresses = util.RetrieveAllPlayerUnusedCards(storytellerId)
                                                    .Select(m => m.FileName)
                                                    .ToList();
                actualResult = sut.RetrievePlayerRemainingCardAddresses(storytellerId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(actualResult.Result);
            var actualRemainingCardAddresses = Assert.IsType<List<string>>(okResult.Value);
            Assert.NotNull(actualRemainingCardAddresses);
            Assert.Equal(expectedRemainigCardAddresses, actualRemainingCardAddresses);
        }
    }
}
