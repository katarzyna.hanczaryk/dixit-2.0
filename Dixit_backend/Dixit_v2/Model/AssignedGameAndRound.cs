﻿namespace Dixit_v2.Model
{
    public class AssignedGameAndRound
    {
        public int RoundId { get; set; }
        public int GameId { get; set; }
    }
}
