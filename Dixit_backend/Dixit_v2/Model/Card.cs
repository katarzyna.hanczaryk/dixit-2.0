﻿using System.ComponentModel.DataAnnotations;

namespace Dixit_v2.Model
{
    public class Card
    {
        [Key]
        public int Id { get; set; }
        public string FileName { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is Card)) return false;

            return (this.FileName == ((Card)obj).FileName
                    && this.Id == ((Card)obj).Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ FileName.GetHashCode();
        }
    }
}
