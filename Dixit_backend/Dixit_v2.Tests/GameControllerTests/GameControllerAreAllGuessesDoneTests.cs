﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerAreAllGuessesDoneTests : IntegrationTests
    {
        private Util util = new();

        [Fact]
        public async Task CanRetrieveTrueForAllGuessesForRound()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            bool expectedResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                result = await sut.AreAllGuessesDone(util.RetrieveValidRoundId());

                expectedResult = _context.PlayerGuessCards.Count() == util.RetrieveRequiredPlayerNumber() - 1;
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<bool>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public async Task CanRetrieveFalseForIncompletGuessesForRound()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            bool expectedResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithNotAllGueesesDone();
                result = await sut.AreAllGuessesDone(util.RetrieveValidRoundId());

                expectedResult = _context.PlayerGuessCards.Count() == util.RetrieveRequiredPlayerNumber() - 1;
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<bool>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public async Task CannotRetrieveResultForInvalidRoundId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                result = await sut.AreAllGuessesDone(util.GenerateInvalidRoundId());
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
