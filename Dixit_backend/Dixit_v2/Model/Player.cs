﻿using System.ComponentModel.DataAnnotations;

namespace Dixit_v2.Model
{
    public class Player
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
