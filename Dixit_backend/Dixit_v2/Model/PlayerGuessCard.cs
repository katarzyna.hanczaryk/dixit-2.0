﻿using System.ComponentModel.DataAnnotations;

namespace Dixit_v2.Model
{
    public class PlayerGuessCard
    {
        [Key]
        public int Id { get; set; }
        public int RoundChosenCardId { get; set; }
        public int PlayerId { get; set; }
    }
}
