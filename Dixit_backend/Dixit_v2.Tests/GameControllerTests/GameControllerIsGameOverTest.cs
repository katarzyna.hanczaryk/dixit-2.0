﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerIsGameOverTest : IntegrationTests
    {
        private Util util = new();

        [Fact]
        public void CanRetrieveTrueWhenGameIsOver()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            bool expectedResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithWinner();
                result = sut.IsGameOver(util.RetrieveValidGameId());
                expectedResult = true;
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<bool>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void CanRetrieveFalseWhenGameIsNotOver()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            bool expectedResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                result = sut.IsGameOver(util.RetrieveValidGameId());

                expectedResult = false;
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<bool>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void CannotRetrieveResultForInvalidGameId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                result = sut.IsGameOver(util.GenerateInvalidGameId());
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
