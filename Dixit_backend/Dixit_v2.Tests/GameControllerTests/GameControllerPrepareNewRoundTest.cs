﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerPrepareNewRoundTest : IntegrationTests
    {
        private Util util = new();

        [Fact]
        public async Task CanPrepareNewRoundWhenPreviousIsOver()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            int previousStorytellerId = 0;
            int expectedRoundId = 0;
            int nextStorytellerId = 0;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                int playerId = util.RetrieveAllPlayersId()
                                   .First();
                result = await sut.PrepareNewRound(gameId, playerId);
                expectedRoundId = util.RetrieveValidRoundId();
                nextStorytellerId = util.RetrieveStorytellerId(expectedRoundId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualRoundId = Assert.IsType<int>(okResult.Value);
            Assert.Equal(expectedRoundId, actualRoundId);
            Assert.NotEqual(previousStorytellerId, nextStorytellerId);
        }

        [Fact]
        public async Task CannotPrepareNewRoundForInvalidGameId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int invalidGameId = util.GenerateInvalidGameId();
                int playerId = util.RetrieveAllPlayersId().First();
                result = await sut.PrepareNewRound(invalidGameId, playerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public async Task CannotPrepareNewRoundForInvalidPlayerId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int invalidPlayerId = util.GenerateInvalidPlayerId();
                int gameId = util.RetrieveValidGameId();
                result = await sut.PrepareNewRound(gameId, invalidPlayerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public async Task CannotPrepareNewRoundForInvalidPlayerIdAndInvalidGameId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int invalidPlayerId = util.GenerateInvalidPlayerId();
                int invalidGameId = util.GenerateInvalidGameId();
                result = await sut.PrepareNewRound(invalidGameId, invalidPlayerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public async Task DoesNotAddNewRoundWhenHasBeenAlreadyAdded()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            var expectedRoundId = 0;
            var expectedStorytellerId = 0;
            var actualStorytellerId = 0;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                var playersList = util.RetrieveAllPlayersId();
                var playerId = playersList[0];
                expectedStorytellerId = playersList[1];
                util.CreateRound(expectedStorytellerId);
                expectedRoundId = util.RetrieveValidRoundId();
                int gameId = util.RetrieveValidGameId();
                result = await sut.PrepareNewRound(gameId, playerId);
                var okActualResult = (OkObjectResult)result.Result;
                actualStorytellerId = util.RetrieveStorytellerId((int)okActualResult.Value);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualRoundId = Assert.IsType<int>(okResult.Value);
            Assert.Equal(expectedRoundId, actualRoundId);
            Assert.Equal(expectedStorytellerId, actualStorytellerId);
        }
    }
}
