﻿using Dixit_v2.Controllers;
using Dixit_v2.Model;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.CardControllerTests
{
    public class CardControllerRetrievePlayerRoundCardsTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public void CanRetrieveSixCardsForValidPlayerAndValidGame()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<Card>> result;
            var playerCards = new List<Card>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                int gameId = util.RetrieveValidGameId();
                playerCards = util.RetrievePlayerCards(playerId);
                result = sut.RetrievePlayerRoundCards(playerId, gameId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var cardsRetrievedForPlayer = Assert.IsType<List<Card>>(okResult.Value);
            Assert.NotNull(cardsRetrievedForPlayer);
            Assert.Equal(playerCards.Count, cardsRetrievedForPlayer.Count);
            Assert.Equal(playerCards, cardsRetrievedForPlayer);
        }

        [Fact]
        public void CannotRetrieveCardsForInvalidPlayerAndValidGame()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<Card>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                int invalidPlayerId = util.GenerateInvalidPlayerId();
                int gameId = util.RetrieveValidGameId();

                result = sut.RetrievePlayerRoundCards(invalidPlayerId, gameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<String>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrieveCardsForValidPlayerAndInvalidGame()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<Card>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                int invalidGameId = util.GenerateInvalidGameId();
                result = sut.RetrievePlayerRoundCards(playerId, invalidGameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<String>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrieveCardsForInvalidPlayerAndInvalidGame()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<Card>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                int invalidPlayerId = util.GenerateInvalidPlayerId();
                int invalidGameId = util.GenerateInvalidGameId();
                result = sut.RetrievePlayerRoundCards(invalidPlayerId, invalidGameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<String>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrievePlayerUsedCards()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<Card>> result;
            var playerCards = new List<Card>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                int gameId = util.RetrieveValidGameId();
                playerCards = util.RetrieveAllPlayerUnusedCards(playerId);
                result = sut.RetrievePlayerRoundCards(playerId, gameId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var cardsRetrievedForPlayer = Assert.IsType<List<Card>>(okResult.Value);
            Assert.NotNull(cardsRetrievedForPlayer);
            Assert.Equal(playerCards.Count, cardsRetrievedForPlayer.Count);
            Assert.Equal(playerCards, cardsRetrievedForPlayer);
        }
    }
}
