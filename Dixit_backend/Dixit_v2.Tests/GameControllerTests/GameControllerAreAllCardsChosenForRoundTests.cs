﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerAreAllCardsChosenForRoundTests : IntegrationTests
    {
        private Util util = new Util();
        [Fact]
        public async Task CanRetrieveTrueForAllCardsChosenForRound()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            bool expectedResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                result = await sut.AreAllCardsChosenForRoundAsync(util.RetrieveValidRoundId());
                expectedResult = _context.RoundChosenCards.Count() == util.RetrieveRequiredPlayerNumber();
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<bool>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public async Task CanRetrieveFalseWhenNotAllCardsChosenForRound()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            bool expectedResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockCompleteGameWithNotAllRoundChosenCards();
                result = await sut.AreAllCardsChosenForRoundAsync(util.RetrieveValidRoundId());
                expectedResult = _context.RoundChosenCards.Count() == util.RetrieveRequiredPlayerNumber();
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<bool>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public async Task CannotRetrieveAllCardsChosenForRoundForInvalidRoundId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int invalidRoundId = util.GenerateInvalidRoundId();
                result = await sut.AreAllCardsChosenForRoundAsync(invalidRoundId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
