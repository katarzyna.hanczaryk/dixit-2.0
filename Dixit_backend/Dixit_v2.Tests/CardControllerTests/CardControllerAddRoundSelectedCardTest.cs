﻿using Dixit_v2.Controllers;
using Dixit_v2.Model;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.CardControllerTests
{
    public class CardControllerAddRoundSelectedCardTest : IntegrationTests
    {
        private Util util = new();

        [Fact]
        public async Task CanAddCardForValidCardIdAndValidRoundId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            RoundChosenCard selectedCard;
            int roundId, playersCardId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                roundId = util.RetrieveValidRoundId();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                playersCardId = util.RetrievePlayerUnusedCardId(playerId);
                result = await sut.AddRoundSelectedCardAsync(new SelectedCard()
                {
                    ChosenCardId = playersCardId,
                    RoundId = roundId
                });
                selectedCard = _context.RoundChosenCards.FirstOrDefault();
                scope.Dispose();
            }
            Assert.IsType<OkResult>(result);
            Assert.NotNull(selectedCard);
            Assert.Equal(playersCardId, selectedCard.CardId);
            Assert.Equal(roundId, selectedCard.RoundId);
        }

        [Fact]
        public async Task CanAddCardForStoryteller()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            RoundChosenCard selectedCard;
            int roundId, playersCardId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                roundId = util.RetrieveValidRoundId();
                int storytellerId = util.RetrieveStorytellerId(roundId);
                playersCardId = util.RetrievePlayerUnusedCardId(storytellerId);
                result = await sut.AddRoundSelectedCardAsync(new SelectedCard()
                {
                    ChosenCardId = playersCardId,
                    RoundId = roundId
                });
                selectedCard = _context.RoundChosenCards.FirstOrDefault();
                scope.Dispose();
            }
            Assert.IsType<OkResult>(result);
            Assert.NotNull(selectedCard);
            Assert.Equal(playersCardId, selectedCard.CardId);
            Assert.Equal(roundId, selectedCard.RoundId);
        }

        [Fact]
        public async Task CanAddCardForNonStoryteller()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            RoundChosenCard selectedCard;
            int roundId, playersCardId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                roundId = util.RetrieveValidRoundId();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                playersCardId = util.RetrievePlayerUnusedCardId(playerId);
                result = await sut.AddRoundSelectedCardAsync(new SelectedCard()
                {
                    ChosenCardId = playersCardId,
                    RoundId = roundId
                });
                selectedCard = _context.RoundChosenCards.FirstOrDefault();
                scope.Dispose();
            }
            Assert.IsType<OkResult>(result);
            Assert.NotNull(selectedCard);
            Assert.Equal(playersCardId, selectedCard.CardId);
            Assert.Equal(roundId, selectedCard.RoundId);
        }

        [Fact]
        public async Task CannotAddCardForInvalidCardIdAndValidRoundId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            RoundChosenCard selectedCard;
            int roundId, invalidCardId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                roundId = util.RetrieveValidRoundId();
                invalidCardId = util.GenerateInvalidCardId();
                result = await sut.AddRoundSelectedCardAsync(new SelectedCard()
                {
                    ChosenCardId = invalidCardId,
                    RoundId = roundId
                });
                selectedCard = _context.RoundChosenCards.FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(selectedCard);
        }

        [Fact]
        public async Task CannotAddCardForValidCardIdAndInvalidRoundId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            RoundChosenCard selectedCard;
            int invalidRoundId, playerCardId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                int roundId = util.RetrieveValidRoundId();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                playerCardId = util.RetrievePlayerUnusedCardId(playerId);
                invalidRoundId = util.GenerateInvalidRoundId();
                result = await sut.AddRoundSelectedCardAsync(new SelectedCard()
                {
                    ChosenCardId = playerCardId,
                    RoundId = invalidRoundId
                });
                selectedCard = _context.RoundChosenCards.FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(selectedCard);
        }

        [Fact]
        public async Task CannotAddCardForInvalidCardIdAndInvalidRoundId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            RoundChosenCard selectedCard;
            int invalidRoundId, invalidPlayerCardId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                invalidRoundId = util.GenerateInvalidRoundId();
                invalidPlayerCardId = util.GenerateInvalidCardId();
                result = await sut.AddRoundSelectedCardAsync(new SelectedCard()
                {
                    ChosenCardId = invalidPlayerCardId,
                    RoundId = invalidRoundId
                });
                selectedCard = _context.RoundChosenCards.FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(selectedCard);
        }
    }
}
