﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.CardControllerTests
{
    public class CardControllerAssignMissingCardTest : IntegrationTests
    {
        private Util util = new();

        [Fact]
        public async Task CanAssignOneCardWhenMissingOne()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            int actualPlayerCardsNumber = 0;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithNoneGuessedStorytellerCard();
                int playerId = util.RetrieveAllPlayersId().First();
                int gameId = util.RetrieveValidGameId();
                result = await sut.AssignMissingPlayerCard(playerId, gameId);

                var playerCardsInGameTotal = _context.CardsInGame
                                                     .Where(m => m.PlayerId == playerId)
                                                     .Select(n => n.CardId);
                var gameRoundsTotal = _context.GameRounds
                                              .Where(m => m.GameId == gameId)
                                              .Select(n => n.RoundId)
                                              .ToList();
                var roundChosenCardsTotal = _context.RoundChosenCards
                                                    .Where(m => gameRoundsTotal.Contains(m.RoundId))
                                                    .Select(n => n.CardId);

                actualPlayerCardsNumber = playerCardsInGameTotal.Except(roundChosenCardsTotal)
                                                                .Count();
                var expectedPlayerCardsNumber = util.RetrieveTotalCardsNumber();
                scope.Dispose();
            }
            Assert.IsType<OkResult>(result);
            Assert.Equal(util.RetrieveTotalCardsNumber(), actualPlayerCardsNumber);
        }

        [Fact]
        public async Task CannotAssignMissingCardForInvalidPlayerId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithNoneGuessedStorytellerCard();
                int gameId = util.RetrieveValidGameId();
                int invalidPlayerId = util.GenerateInvalidPlayerId();
                result = await sut.AssignMissingPlayerCard(invalidPlayerId, gameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public async Task CannotAssignMissingCardForInvalidGameId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithNoneGuessedStorytellerCard();
                int invalidGameId = util.GenerateInvalidGameId();
                int playerId = util.RetrieveAllPlayersId().First();
                result = await sut.AssignMissingPlayerCard(playerId, invalidGameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public async Task CannotAssignCardsWhenCompleteCardsNumber()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                int gameId = util.GenerateInvalidGameId();
                int playerId = util.RetrieveAllPlayersId().First();
                result = await sut.AssignMissingPlayerCard(playerId, gameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
        }

    }
}
