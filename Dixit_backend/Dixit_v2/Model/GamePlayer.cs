﻿using System.ComponentModel.DataAnnotations;

namespace Dixit_v2.Model
{
    public class GamePlayer
    {
        [Key]
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public int GameId { get; set; }
        public byte Order { get; set; }
    }
}
