﻿using AutoFixture.Xunit2;
using Dixit_v2.Controllers;
using Dixit_v2.Model;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.CardControllerTests
{
    public class CardControllerAddDescriptionTest : IntegrationTests
    {
        public Util util = new();
        [Theory]
        [AutoData]
        public async Task CanAddDescriptionForValidRoundId(string cardDescription)
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            CardDescription addedCardDescription;
            int roundId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                roundId = util.RetrieveValidRoundId();
                result = await sut.AddDescriptionAsync(new Description()
                {
                    Hint = cardDescription,
                    RoundId = roundId
                });
                addedCardDescription = _context.CardDescriptions
                                               .FirstOrDefault();
                scope.Dispose();
            }
            Assert.IsType<OkResult>(result);
            Assert.NotNull(addedCardDescription);
            Assert.Equal(cardDescription, addedCardDescription.Description);
            Assert.Equal(roundId, addedCardDescription.RoundId);
        }

        [Theory]
        [AutoData]
        public async Task CannotAddDescriptionForInvalidRoundId(string cardDescription)
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            CardDescription addedCardDescription;
            int invalidRoundId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                invalidRoundId = util.GenerateInvalidRoundId();
                result = await sut.AddDescriptionAsync(new Description()
                {
                    Hint = cardDescription,
                    RoundId = invalidRoundId
                });
                addedCardDescription = _context.CardDescriptions.FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(addedCardDescription);
        }

        [Fact]
        public async Task CannotAddDescriptionForValidRoundIdAndInvalidDescription()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            CardDescription addedCardDescription;
            string cardDescription = null;
            int validRoundId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                validRoundId = util.RetrieveValidRoundId();
                result = await sut.AddDescriptionAsync(new Description()
                {
                    Hint = cardDescription,
                    RoundId = validRoundId
                });
                addedCardDescription = _context.CardDescriptions.FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(addedCardDescription);
        }

        [Fact]
        public async Task CannotAddDescriptionForInvalidRoundIdAndInvalidDescription()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            CardDescription addedCardDescription;
            string cardDescription = null;
            int invalidRoundId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                invalidRoundId = util.GenerateInvalidRoundId();
                result = await sut.AddDescriptionAsync(new Description()
                {
                    Hint = cardDescription,
                    RoundId = invalidRoundId
                });
                addedCardDescription = _context.CardDescriptions.FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(addedCardDescription);
        }
    }
}
