﻿using Dixit_v2.Controllers;
using Dixit_v2.Model;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.CardControllerTests
{
    public class CardControllerAddGuessTest : IntegrationTests
    {
        private Util util = new Util();
        [Fact]
        public async Task CanAddGuessForValidParameters()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            PlayerGuessCard expectedResult, actualResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int roundId = util.RetrieveValidRoundId();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                int cardId = util.RetrievePlayerGuessCard(playerId);
                result = await sut.AddGuessCardAsync(new AddGuessParams()
                {
                    RoundId = roundId,
                    PlayerId = playerId,
                    CardId = cardId
                });
                actualResult = _context.PlayerGuessCards
                                       .Where(m => m.PlayerId == playerId)
                                       .FirstOrDefault();
                int expectedRoundChosenCardId = _context.RoundChosenCards
                                                        .Where(m => m.RoundId == roundId && m.CardId == cardId)
                                                        .Select(m => m.Id)
                                                        .FirstOrDefault();
                expectedResult = new PlayerGuessCard()
                {
                    RoundChosenCardId = expectedRoundChosenCardId,
                    PlayerId = playerId
                };
                scope.Dispose();
            }
            Assert.IsType<OkResult>(result);
            Assert.NotNull(actualResult);
            Assert.Equal(expectedResult.RoundChosenCardId, actualResult.RoundChosenCardId);
            Assert.Equal(expectedResult.PlayerId, actualResult.PlayerId);
        }

        [Fact]
        public async Task CannotAddGuessForStoryteller()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            PlayerGuessCard actualResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int roundId = util.RetrieveValidRoundId();
                int storytellerId = util.RetrieveStorytellerId(roundId);
                int cardId = util.RetrievePlayerGuessCard(storytellerId);
                result = await sut.AddGuessCardAsync(new AddGuessParams()
                {
                    RoundId = roundId,
                    PlayerId = storytellerId,
                    CardId = cardId
                });
                actualResult = _context.PlayerGuessCards
                                       .Where(m => m.PlayerId == storytellerId)
                                       .FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(actualResult);
        }

        [Fact]
        public async Task CannotAddGuessForInvalidPlayerId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            PlayerGuessCard actualResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int roundId = util.RetrieveValidRoundId();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                int cardId = util.RetrievePlayerGuessCard(playerId);

                int invalidPlayerId = util.GenerateInvalidPlayerId();
                result = await sut.AddGuessCardAsync(new AddGuessParams()
                {
                    RoundId = roundId,
                    PlayerId = invalidPlayerId,
                    CardId = cardId
                });
                actualResult = _context.PlayerGuessCards
                                       .Where(m => m.PlayerId == invalidPlayerId)
                                       .FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(actualResult);
        }

        [Fact]
        public async Task CannotAddGuessForInvalidCardId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            PlayerGuessCard actualResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int roundId = util.RetrieveValidRoundId();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                int invalidCardId = util.GenerateInvalidCardId();
                result = await sut.AddGuessCardAsync(new AddGuessParams()
                {
                    RoundId = roundId,
                    PlayerId = playerId,
                    CardId = invalidCardId
                });
                actualResult = _context.PlayerGuessCards
                                       .Where(m => m.PlayerId == playerId)
                                       .FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(actualResult);
        }

        [Fact]
        public async Task CannotAddGuessForInvalidRoundId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            PlayerGuessCard actualResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                int cardId = util.RetrievePlayerGuessCard(playerId);
                int invalidRoundId = util.GenerateInvalidRoundId();
                result = await sut.AddGuessCardAsync(new AddGuessParams()
                {
                    RoundId = invalidRoundId,
                    PlayerId = playerId,
                    CardId = cardId
                });
                actualResult = _context.PlayerGuessCards
                                       .Where(m => m.PlayerId == playerId)
                                       .FirstOrDefault();
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
            Assert.Null(actualResult);
        }

        [Fact]
        public async Task AllNormalPlayersCanAddGuessPerRound()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            PlayerGuessCard expectedResult, actualResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int roundId = util.RetrieveValidRoundId();
                int storytellerId = util.RetrieveStorytellerId(roundId);
                int lastPlayerId = _context.Players
                           .Where(m => m.Id != storytellerId)
                           .OrderBy(m => m.Id)
                           .Select(n => n.Id)
                           .LastOrDefault();
                int cardId = util.RetrievePlayerGuessCard(lastPlayerId);
                result = await sut.AddGuessCardAsync(new AddGuessParams()
                {
                    RoundId = roundId,
                    PlayerId = lastPlayerId,
                    CardId = cardId
                });
                actualResult = _context.PlayerGuessCards
                                       .Where(m => m.PlayerId == lastPlayerId)
                                       .FirstOrDefault();
                int expectedRoundChosenCardId = _context.RoundChosenCards
                                                        .Where(n => n.CardId == cardId
                                                            && n.RoundId == roundId)
                                                        .Select(m => m.Id)
                                                        .FirstOrDefault();
                expectedResult = new PlayerGuessCard()
                {
                    RoundChosenCardId = expectedRoundChosenCardId,
                    PlayerId = lastPlayerId
                };
                scope.Dispose();
            }
            Assert.IsType<OkResult>(result);
            Assert.NotNull(actualResult);
            Assert.Equal(expectedResult.RoundChosenCardId, actualResult.RoundChosenCardId);
            Assert.Equal(expectedResult.PlayerId, actualResult.PlayerId);
        }
    }
}
