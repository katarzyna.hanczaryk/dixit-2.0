﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.PlayerControllerTests
{
    public class PlayerControllerIsPlayerStorytellerTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public void CanRetrieveTrueWhenPlayerIsStoryteller()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int roundId = util.RetrieveValidRoundId();
                int playerId = util.RetrieveStorytellerId(roundId);
                result = sut.IsPlayerStoryteller(roundId, playerId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<bool>(okResult.Value);
            Assert.True(actualResult);
        }

        [Fact]
        public void CanRetrieveTrueWhenPlayerIsNotStoryteller()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int playerId = util.RetrieveNonStorytellerPlayerId();
                result = sut.IsPlayerStoryteller(util.RetrieveValidRoundId(), playerId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<bool>(okResult.Value);
            Assert.False(actualResult);
        }

        [Fact]
        public void CannotRetrieveResultForInvalidRoundId()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int playerId = util.RetrieveStorytellerId(util.RetrieveValidRoundId());
                int invalidRoundId = util.GenerateInvalidRoundId();
                result = sut.IsPlayerStoryteller(invalidRoundId, playerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrieveResultForInvalidPlayerId()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int invalidPlayerId = util.GenerateInvalidPlayerId();
                int roundId = util.RetrieveValidRoundId();
                result = sut.IsPlayerStoryteller(roundId, invalidPlayerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrieveResultForInvalidPlayerIdAndInvalidRoundId()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<bool> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                int invalidPlayerId = util.GenerateInvalidPlayerId();
                int invalidRoundId = util.GenerateInvalidRoundId();
                result = sut.IsPlayerStoryteller(invalidRoundId, invalidPlayerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
