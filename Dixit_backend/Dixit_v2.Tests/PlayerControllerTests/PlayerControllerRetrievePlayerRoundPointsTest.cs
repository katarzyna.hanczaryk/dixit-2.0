﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.PlayerControllerTests
{
    public class PlayerControllerRetrievePlayerRoundPointsTest : IntegrationTests
    {
        private Util util = new();

        [Fact]
        public void CanRetrievePointsForValidRoundIdAndValidPlayerIdBeforeRoundIsOver()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            int expectedResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                var roundId = util.RetrieveValidRoundId();
                var playerId = util.RetrieveNonStorytellerPlayerId();

                expectedResult = 0;
                result = sut.RetrievePlayerRoundPoints(roundId, playerId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<Int32>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void CanRetrievePointsForValidRoundIdAndValidPlayerIdWhenRoundIsOver()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            int expectedResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                var roundId = util.RetrieveValidRoundId();
                var playerId = util.RetrieveNonStorytellerPlayerId();

                expectedResult = util.RetrieveBaseMediumPoints();
                result = sut.RetrievePlayerRoundPoints(roundId, playerId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<Int32>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void CanRetrievePointsForValidRoundIdForStorytellerWhenRoundIsOver()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            int expectedResult;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                var roundId = util.RetrieveValidRoundId();
                var playerId = util.RetrieveStorytellerId(roundId);

                expectedResult = util.RetrieveBaseMinimumPoints();
                result = sut.RetrievePlayerRoundPoints(roundId, playerId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<Int32>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void CannotRetrievePointsForValidRoundIdAndInvalidPlayerIdWhenRoundIsOver()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                var roundId = util.RetrieveValidRoundId();
                var invalidPlayerId = util.GenerateInvalidPlayerId();

                result = sut.RetrievePlayerRoundPoints(roundId, invalidPlayerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrievePointsForInvalidRoundIdAndValidPlayerIdWhenRoundIsOver()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                var invalidRoundId = util.GenerateInvalidRoundId();
                var playerId = util.RetrieveNonStorytellerPlayerId();

                result = sut.RetrievePlayerRoundPoints(invalidRoundId, playerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrievePointsForInvalidRoundIdAndInvalidPlayerIdWhenRoundIsOver()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllGuessedStorytellerCard();
                var invalidRoundId = util.GenerateInvalidRoundId();
                var invalidPlayerId = util.GenerateInvalidPlayerId();

                result = sut.RetrievePlayerRoundPoints(invalidRoundId, invalidPlayerId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
