﻿using Dixit_v2.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dixit_v2.Common
{
    public abstract class BaseApiController<TClass> : Controller
    {
        protected readonly ApplicationDbContext _db;
        protected readonly ILogger _logger;
        protected readonly IConfiguration _configuration;

        protected BaseApiController(ApplicationDbContext db, ILoggerFactory loggerFactory, IConfiguration iConfig)
        {
            _db = db;
            _logger = loggerFactory.CreateLogger<TClass>();
            _configuration = iConfig;
        }

        protected IEnumerable<string> RetrieveRemainingCardAddresses(int playerId)
        {
            bool selectPlayerCards(Card card) => _db.CardsInGame
                                                 .Where(cardInGame => cardInGame.PlayerId == playerId)
                                                 .Select(cardInGame => cardInGame.CardId)
                                                 .Contains(card.Id);

            var remainingCardsIds = _db.Cards
                                         .Where(selectPlayerCards)
                                         .Select(card => card.Id)
                                         .Except(_db.RoundChosenCards.Select(n => n.CardId));

            return _db.Cards.Where(m => remainingCardsIds
                                        .Contains(m.Id))
                                        .Select(n => n.FileName)
                                        .ToList();
        }

        protected async Task GenerateRandomPlayerCardsAsync(int playerId, int gameId)
        {
            int seed = GetSeed();
            int totalCards = GetTotalCards();
            Random r = new(seed++);
            ISet<int> randomIds = new HashSet<int>(totalCards);
            IEnumerable<int> playerIds = GetTotalGamePlayerIds(gameId);
            IEnumerable<int> cardsInGameIds = _db.CardsInGame
                                                .Where(m => playerIds.Contains(m.PlayerId))
                                                .Select(n => n.CardId);
            while (randomIds.Count < totalCards)
            {
                int generatedRandomId = r.Next(1, _db.Cards.Count());
                // to make sure that all assigned cards are unique for the game
                if (!cardsInGameIds.Contains(generatedRandomId))
                    randomIds.Add(generatedRandomId);
            }
            IEnumerable<Card> cards = _db.Cards.Where(m => randomIds.Contains(m.Id));

            await _db.CardsInGame.AddRangeAsync(cards.Select(n => new CardInGame()
            {
                CardId = n.Id,
                PlayerId = playerId
            }));
            await _db.SaveChangesAsync();
        }

        protected IEnumerable<Card> RetrieveAllPlayerCards(int playerId, int gameId)
        {
            bool isPlayerInGame = _db.GamePlayers
                                     .Where(m => m.GameId == gameId &&
                                                 m.PlayerId == playerId)
                                     .Any();
            if (isPlayerInGame)
            {
                IEnumerable<int> totalRounds = _db.GameRounds
                                                  .Where(m => m.GameId == gameId)
                                                  .Select(n => n.RoundId);
                IEnumerable<int> totalGameUsedCards = _db.RoundChosenCards
                                                         .Where(m => totalRounds.Contains(m.RoundId))
                                                         .Select(n => n.CardId);
                IEnumerable<int> totalCardIds = _db.CardsInGame
                                                   .Where(m => m.PlayerId == playerId)
                                                   .Select(n => n.CardId)
                                                   .Except(totalGameUsedCards);
                var result = _db.Cards
                          .Where(m => totalCardIds.Contains(m.Id)).ToList();
                return result;
            }
            throw new Exception("Incorrect playerId-gameId combination");
        }

        protected async Task<string> RetrieveHintAsync(int roundId)
        {
            bool isValidRoundId = await _db.Rounds
                                           .Where(m => m.Id == roundId)
                                           .AnyAsync();
            if (!isValidRoundId)
                throw new Exception("Incorrect roundId");
            if (await IsHintAddedAsync(roundId))
                return _db.CardDescriptions
                           .Where(n => n.RoundId == roundId)
                           .Select(m => m.Description)
                           .FirstOrDefault();
            return null;
        }

        protected async Task AddRoundChosenCardAsync(int chosenCardId, int roundId)
        {
            await _db.RoundChosenCards.AddAsync(new RoundChosenCard()
            {
                CardId = chosenCardId,
                RoundId = roundId
            });
            await _db.SaveChangesAsync();
        }

        protected async Task<string> RetrievePlayerChosenCardAsync(int chosenCardId)
        {
            return await _db.Cards
                            .Where(m => m.Id == chosenCardId)
                            .Select(n => n.FileName)
                            .FirstOrDefaultAsync();
        }

        protected IEnumerable<Card> RetrieveRoundSelectedCards(int roundId)
        {
            var result = _db.Cards
                           .Where(m => _db.RoundChosenCards
                                 .Where(m => m.RoundId == roundId)
                                 .Select(n => n.CardId)
                           .Contains(m.Id))
                           .ToList();
            int playersNumber = GetPlayersNumber();
            if (result.Count == playersNumber)
                return result;
            throw new Exception("Not all players selected cards in this round yet.");
        }

        protected async Task<int> RetrievePlayerNumberAsync(int gameId)
        {
            return await _db.GamePlayers
                      .Where(m => m.GameId == gameId)
                      .CountAsync();
        }

        protected async Task<Card> GetStorytellerCardAsync(int roundId)
        {
            bool roundExists = _db.Rounds
                                  .Where(m => m.Id == roundId)
                                  .Any();
            if (roundExists)
            {
                var roundChosenCardIds = _db.RoundChosenCards
                                            .Where(m => m.RoundId == roundId)
                                            .Select(m => m.CardId);

                var roundStorytellerCard = await _db.CardsInGame
                                                .Where(m => m.PlayerId == _db.Rounds
                                                                            .Where(m => m.Id == roundId)
                                                                            .First()
                                                                            .StorytellerId
                                                                            && roundChosenCardIds.Contains(m.CardId))
                                                .FirstAsync();
                int roundStorytellerCardId = roundStorytellerCard.CardId;
                return await _db.Cards.Where(m => m.Id == roundStorytellerCardId)
                                                  .FirstAsync();
            }
            else
                throw new Exception("Given roundId invalid.");
        }

        protected int GetCurrentStorytellerId(int roundId)
        {
            return _db.Rounds
                      .Where(m => m.Id == roundId)
                      .First()
                      .StorytellerId;
        }

        protected IEnumerable<int> GetTotalGamePlayerIds(int gameId)
        {
            IEnumerable<int> result = _db.GamePlayers
                                         .Where(m => m.GameId == gameId)
                                         .Select(n => n.PlayerId);
            if (result.Any()) return result;
            throw new Exception();
        }

        protected async Task<int> RetrieveRoundGuessesNumberAsync(int roundId)
        {
            IEnumerable<int> currentRoundChosenCardsIds = _db.RoundChosenCards
                                                            .Where(m => m.RoundId == roundId)
                                                            .Select(n => n.Id);
            return await _db.PlayerGuessCards
                     .Where(m => currentRoundChosenCardsIds.Contains(m.RoundChosenCardId))
                     .CountAsync();
        }

        protected int GenerateUniqueRandomCardId(int gameId)
        {
            Random r = new Random(GetSeed() + 1);
            int uniqueRandomCardId = 0;
            while (uniqueRandomCardId == 0)
            {
                int generatedRandomId = r.Next(1, 85);
                // to make sure that all assigned cards are unique for the game
                IEnumerable<int> playerIds = GetTotalGamePlayerIds(gameId);
                IEnumerable<int> cardsInGameIds = _db.CardsInGame
                                                    .Where(m => playerIds.Contains(m.PlayerId))
                                                    .Select(n => n.CardId);
                if (!cardsInGameIds.Contains(generatedRandomId))
                    uniqueRandomCardId = generatedRandomId;
            }
            return uniqueRandomCardId;
        }

        protected IEnumerable<PlayersPoints> RetrievePlayersRoundAndGamePoints(int gameId)
        {
            List<int> players = GetTotalGamePlayerIds(gameId).ToList();
            IEnumerable<DataForPointsCalculation> dataForPointsCalculations = _db.vwDataForPointsCalculation.Where(m => m.GameId == gameId);
            IEnumerable<(int playerId, int totalPoints)> totalPoints = CalculateTotalPoints(dataForPointsCalculations);
            int lastRoundId = dataForPointsCalculations.GroupBy(m => m.RoundId)
                                                       .OrderByDescending(n => n.Key)
                                                       .SkipWhile(group => group.Count() < players.Count() || group.Where(k => k.GuessedCardId == null).Count() > 1)
                                                       .Select(l => l.Key)
                                                       .FirstOrDefault();
            IEnumerable<DataForPointsCalculation> lastRoundData = dataForPointsCalculations.Where(m => m.RoundId == lastRoundId);
            IEnumerable<(int playerId, int roundPoints)> lastRoundPoints = CalculateRoundPoints(lastRoundData);
            return
                players.Select(m => new PlayersPoints()
                {
                    PlayerName = _db.Players.Where(n => n.Id == m)
                                           .Select(k => k.Name)
                                           .FirstOrDefault(),
                    RoundPoints = lastRoundPoints.Where(n => n.playerId == m)
                                                 .Select(k => k.roundPoints)
                                                 .FirstOrDefault(),
                    GamePoints = totalPoints.Where(n => n.playerId == m)
                                            .Select(k => k.totalPoints)
                                            .FirstOrDefault()
                }).ToList();
        }

        protected int CalculateGameMaxPointsNumber(int gameId)
        {
            if (GameExists(gameId))
            {
                IEnumerable<DataForPointsCalculation> dataForPointsCalculations = _db.vwDataForPointsCalculation
                                                                                    .Where(m => m.GameId == gameId);
                IEnumerable<(int playerId, int totalPoints)> playersTotalPoints = CalculateTotalPoints(dataForPointsCalculations);
                return playersTotalPoints.Max(m => m.totalPoints);
            }
            else
                throw new Exception("Invalid gameId.");
        }

        protected async Task AddCardDescriptionAsync(int roundId, string cardDescription)
        {
            await _db.CardDescriptions.AddAsync(new CardDescription
            {
                Description = cardDescription,
                RoundId = roundId
            });
            await _db.SaveChangesAsync();
        }

        protected async Task AddPlayerGuessCardAsync(int playerId, int roundId, int id)
        {
            bool isStoryteller = GetCurrentStorytellerId(roundId) == playerId;
            if (!isStoryteller)
            {
                await _db.PlayerGuessCards.AddAsync(new PlayerGuessCard
                {
                    PlayerId = playerId,
                    RoundChosenCardId = _db.RoundChosenCards
                                                .Where(m => m.CardId == id
                                                             && m.RoundId == roundId)
                                                .First().Id
                });
                await _db.SaveChangesAsync();
            }
            else
                throw new Exception("Storyteller cannot guess a card.");
        }

        protected async Task<int> AddNewPlayerAsync(string name)
        {
            if (!String.IsNullOrEmpty(name))
            {
                var id = new SqlParameter()
                {
                    ParameterName = "@Identity",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output
                };
                await _db.Database.ExecuteSqlInterpolatedAsync($"EXEC [dbo].[spAddNewPlayer] {name}, {id} OUTPUT");
                return (int)id.Value;
            }
            else
            {
                throw new Exception("Name is null or empty");
            }
        }

        protected async Task AssignPlayerMissingCardsAsync(int playerId, int gameId)
        {
            if (GameExists(gameId) && PlayerExists(playerId))
            {
                await _db.CardsInGame
                         .AddAsync(new CardInGame
                         {
                             PlayerId = playerId,
                             CardId = GenerateUniqueRandomCardId(gameId)
                         });
                await _db.SaveChangesAsync();
            }
            else
                throw new Exception("Given game or player ids aren't correct.");
        }

        protected async Task<int> AddNewRoundAndChangeStorytellerAsync(int playerId, int gameId)
        {
            if (PlayerExists(playerId) && GameExists(gameId))
            {
                var currentRoundId = new SqlParameter
                {
                    ParameterName = "@CurrentRoundId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output
                };
                await _db.Database
                    .ExecuteSqlInterpolatedAsync($@"EXEC [dbo].[spAddNewRoundAndNewStoryteller] {playerId}, 
                                                        {gameId}, {currentRoundId} OUTPUT");
                return (int)currentRoundId.Value;
            }
            else
                throw new Exception("Incorrect parameters.");
        }

        protected async Task<AssignedGameAndRound> AssignToGameAsync(int playerId)
        {
            var gameId = new SqlParameter
            {
                ParameterName = "@GameIdToAssign",
                SqlDbType = System.Data.SqlDbType.Int,
                Direction = System.Data.ParameterDirection.Output
            };
            var roundId = new SqlParameter
            {
                ParameterName = "@RoundId",
                SqlDbType = System.Data.SqlDbType.Int,
                Direction = System.Data.ParameterDirection.Output
            };
            await _db.Database
                .ExecuteSqlInterpolatedAsync($"EXEC [dbo].[spAssignPlayerToGame] {playerId}, {gameId} OUTPUT, {roundId} OUTPUT");
            return new AssignedGameAndRound()
            {
                RoundId = (int)roundId.Value,
                GameId = (int)gameId.Value
            };
        }

        protected async Task<bool> AreAllCardsPerRoundChosenAsync(int roundId)
        {
            bool roundIdExists = _db.Rounds
                                    .Where(m => m.Id == roundId)
                                    .Any();
            if (roundIdExists)
            {
                var roundChosenCards = await _db.RoundChosenCards
                                                .Where(m => m.RoundId == roundId)
                                                .CountAsync();
                var playersNumber = GetPlayersNumber();
                return roundChosenCards == playersNumber;
            }
            else
                throw new Exception("Incorrect roundId");
        }

        protected async Task<bool> AllRoundGuessesDone(int roundId)
        {
            if (RoundExists(roundId))
            {
                int currentNumberOfGuesses = await RetrieveRoundGuessesNumberAsync(roundId);
                int expectedNumberOfGuesses = GetPlayersNumber() - 1;
                return currentNumberOfGuesses == expectedNumberOfGuesses;
            }
            else
                throw new Exception("Invalid roundId given.");
        }

        protected int GetPlayerRoundPoints(int roundId, int playerId)
        {
            if (RoundExists(roundId) && PlayerExists(playerId))
            {
                IEnumerable<DataForPointsCalculation> dataForRoundPoints = _db.vwDataForPointsCalculation
                                                                              .Where(m => m.RoundId == roundId);
                var roundPoints = CalculateRoundPoints(dataForRoundPoints);
                return roundPoints.Where(m => m.playerId == playerId)
                                  .Select(n => n.roundPoints)
                                  .FirstOrDefault();
            }
            else
                throw new Exception("Invalid playerId or roundId given.");
        }

        public IEnumerable<FinalPoints> GetFinalPoints(int gameId)
        {
            if (GameExists(gameId) && IsGameCompleted(gameId))
            {
                var dataForPointsCalculation = _db.vwDataForPointsCalculation.Where(m => m.GameId == gameId);
                var resultTuple = CalculateTotalPoints(dataForPointsCalculation).ToList();
                var result = new List<FinalPoints>();
                resultTuple.ForEach(n => result.Add(new FinalPoints()
                {
                    GamePoints = n.totalPoints,
                    PlayerName = RetrievePlayerName(n.playerId)
                }));
                return result;
            }
            else
                throw new Exception("GameId doesn't exist or game is not completed.");
        }

        protected int GetWinningNumberPoints()
        {
            return Int32
                   .Parse(_configuration
                   .GetSection("GameRules")
                   .GetSection("WinningPoints")
                   .Value);
        }

        protected bool IsGameCompleted(int gameId)
        {
            return CalculateGameMaxPointsNumber(gameId) >= GetWinningNumberPoints();
        }

        protected IEnumerable<string> GetWinnerPlayerNames(int gameId)
        {
            if (GameExists(gameId) && IsGameCompleted(gameId))
            {
                var finalPoints = CalculateTotalPoints(GetDataForGamePointsCalculations(gameId));
                int maxPoints = finalPoints.Select(m => m.totalPoints)
                                           .Max();
                var winnerIds = finalPoints.Where(m => m.totalPoints == maxPoints)
                                           .Select(n => n.playerId)
                                           .ToList();
                return _db.Players
                          .Where(m => winnerIds.Contains(m.Id))
                          .Select(n => n.Name)
                          .ToList();
            }
            else
                throw new Exception("The game is not over yet.");
        }

        protected bool IsStoryteller(int roundId, int playerId)
        {
            if (RoundExists(roundId) && PlayerExists(playerId))
                return GetCurrentStorytellerId(roundId) == playerId;
            else
                throw new Exception("Incorrect parameters given.");
        }

        private bool GameExists(int gameId)
        {
            return _db.Games
                      .Where(m => m.Id == gameId)
                      .Any();
        }

        private bool RoundExists(int roundId)
        {
            return _db.Rounds
                      .Where(m => m.Id == roundId)
                      .Any();
        }

        private bool PlayerExists(int playerId)
        {
            return _db.Players
                      .Where(m => m.Id == playerId)
                      .Any();
        }
        //to calculate points per a given roundId for all players
        private IEnumerable<(int playerId, int roundPoints)> CalculateRoundPoints(IEnumerable<DataForPointsCalculation> dataForRoundPoints)
        {
            var roundPlayerPoints = new List<(int, int)>();
            // the first time, points are calculated when there is only one record in the _db for the game
            if (dataForRoundPoints.Count() > 1)
            {
                int storyTellerId = dataForRoundPoints.Select(m => m.StorytellerId)
                                                            .FirstOrDefault();
                int stortytellerCardId = dataForRoundPoints.Where(m => m.SelectedByPlayer == storyTellerId)
                                                                 .Select(n => n.CardId)
                                                                 .First();
                int guessersNumbers = dataForRoundPoints.Where(n => n.GuessedCardId != null)
                                                        .Select(m => m.GuessedCardId)
                                                        .Count();
                int storytellerCardsGuessedNumber = dataForRoundPoints.Where(m => m.GuessedCardId == stortytellerCardId)
                                                                            .Count();
                var playersIds = dataForRoundPoints.Select(m => m.SelectedByPlayer)
                                                                 .Distinct();
                bool nobodyGuessed = storytellerCardsGuessedNumber == 0;
                bool everybodyGuessed = storytellerCardsGuessedNumber == guessersNumbers;
                bool storytellerLost = nobodyGuessed || everybodyGuessed;

                foreach (int playerId in playersIds)
                {
                    bool isStoryteller = storyTellerId == playerId;
                    bool playerHasGuessed = dataForRoundPoints.Where(m => m.GuessedCardId == stortytellerCardId
                                                                      && m.SelectedByPlayer == playerId)
                                                              .Any();
                    int playerBasePoints = CalculatePlayerBasePoints(isStoryteller, storytellerLost, playerHasGuessed);
                    int playerExtraPoints = 0;
                    // extra points are calculated only if the storyteller hasn't lost and for players excluding storyteller
                    if (!storytellerLost && !isStoryteller)
                    {
                        int playerSelectedCardId = dataForRoundPoints.Where(m => m.SelectedByPlayer == playerId)
                                                                 .Select(n => n.CardId)
                                                                 .FirstOrDefault();
                        playerExtraPoints = dataForRoundPoints.Where(m => m.GuessedCardId == playerSelectedCardId)
                                                              .Count();
                    }
                    int playerPointsSum = playerBasePoints + playerExtraPoints;
                    var playerData = (player: playerId, playerPoints: playerPointsSum);
                    roundPlayerPoints.Add(playerData);
                }
            }
            return roundPlayerPoints;
        }

        private int CalculatePlayerBasePoints(bool isStoryteller, bool hasStorytellerLost, bool playerHasGuessed)
        {
            if (isStoryteller && hasStorytellerLost)
                return GetBaseMinPoints();
            if (isStoryteller && !hasStorytellerLost)
                return GetBaseMaxPoints();
            if (!isStoryteller && hasStorytellerLost)
                return GetBaseMediumPoints();
            else
                return playerHasGuessed ? GetBaseMaxPoints() : GetBaseMinPoints();
        }

        private IEnumerable<(int playerId, int totalPoints)> CalculateTotalPoints(IEnumerable<DataForPointsCalculation> dataForPointsCalculations)
        {
            return dataForPointsCalculations.GroupBy(m => m.RoundId)
                                            .OrderByDescending(n => n.Key)
                                            .SkipWhile(group => group.Where(k => k.GuessedCardId == null).Count() > 1)
                                            .Select(group => CalculateRoundPoints(group))
                                            .SelectMany(x => x).GroupBy(n => n.playerId)
                                            .Select(player => ValueTuple.Create(
                                                                             player.Key,
                                                                             player.Sum(k => k.roundPoints)));
        }

        private async Task<bool> IsHintAddedAsync(int roundId)
        {
            return await _db.CardDescriptions
                            .Where(n => n.RoundId == roundId)
                            .AnyAsync();
        }

        private int GetSeed()
        {
            return Int32
                   .Parse(_configuration
                   .GetSection("GameRules")
                   .GetSection("Seed")
                   .Value);
        }

        private int GetTotalCards()
        {
            return Int32
                   .Parse(_configuration
                   .GetSection("GameRules")
                   .GetSection("TotalCards")
                   .Value);
        }

        private int GetPlayersNumber()
        {
            return Int32
                   .Parse(_configuration
                   .GetSection("GameRules")
                   .GetSection("RequiredPlayersNumber")
                   .Value);
        }

        private int GetBaseMaxPoints()
        {
            return Int32
                   .Parse(_configuration
                   .GetSection("GameRules")
                   .GetSection("BaseMaxPoints")
                   .Value);
        }

        private int GetBaseMediumPoints()
        {
            return Int32
                   .Parse(_configuration
                   .GetSection("GameRules")
                   .GetSection("BaseMediumPoints")
                   .Value);
        }

        private int GetBaseMinPoints()
        {
            return Int32
                   .Parse(_configuration
                   .GetSection("GameRules")
                   .GetSection("BaseMinPoints")
                   .Value);
        }
        private int GetExtraSinglePoints()
        {
            return Int32
                   .Parse(_configuration
                   .GetSection("GameRules")
                   .GetSection("ExtraSinglePoints")
                   .Value);
        }

        private string RetrievePlayerName(int playerId)
        {
            return _db.Players
                      .Where(m => m.Id == playerId)
                      .Select(n => n.Name)
                      .FirstOrDefault();
        }

        private IEnumerable<DataForPointsCalculation> GetDataForGamePointsCalculations(int gameId)
        {
            return _db.vwDataForPointsCalculation
                      .Where(m => m.GameId == gameId);
        }
    }
}
