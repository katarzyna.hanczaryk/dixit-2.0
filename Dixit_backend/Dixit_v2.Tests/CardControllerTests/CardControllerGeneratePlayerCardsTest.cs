﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.CardControllerTests
{
    public class CardControllerGeneratePlayerCardsTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public async Task CanAssignCardsForValidPlayerIdAndGameId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                int playerId = util.CreatePlayer();
                util.CreateGameAndRound(playerId);
                int gameId = util.RetrieveValidGameId();
                result = await sut.GeneratePlayerCardsAsync(playerId, gameId);
                scope.Dispose();
            }
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task CannotAssignCardsForInvalidPlayerIdAndValidGameId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                int gameId = util.RetrieveValidGameId();
                int invalidPlayerId = util.GenerateInvalidPlayerId();
                result = await sut.GeneratePlayerCardsAsync(invalidPlayerId, gameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public async Task CannotAssignCardsForValidPlayerIdAndInvalidGameId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                int playerId = util.CreatePlayer();
                util.CreateGameAndRound(playerId);
                var invalidGameId = util.GenerateInvalidGameId();
                result = await sut.GeneratePlayerCardsAsync(playerId, invalidGameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public async Task CannotAssignCardsForInvalidPlayerIdAndInvalidGameId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                var invalidPlayerId = util.GenerateInvalidPlayerId();
                var invalidGameId = util.GenerateInvalidGameId();
                result = await sut.GeneratePlayerCardsAsync(invalidPlayerId, invalidGameId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public async Task AssignedCardsAreUniquePerPlayer()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            IEnumerable<int> playersCardsInGame = new List<int>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                int playerId = util.CreatePlayer();
                util.CreateGameAndRound(playerId);
                int gameId = util.RetrieveValidGameId();
                await sut.GeneratePlayerCardsAsync(playerId, gameId);
                playersCardsInGame = _context.CardsInGame
                                             .Where(m => m.PlayerId == playerId)
                                             .Select(n => n.CardId);
                scope.Dispose();
            }
            var diffChecker = new HashSet<int>();
            Assert.True(playersCardsInGame.All(diffChecker.Add));
        }

        [Fact]
        public void AssignedCardsAreUniquePerGame()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            var cardsInGame = new List<int>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsGeneration();
                cardsInGame = _context.CardsInGame
                                      .Select(m => m.CardId)
                                      .ToList();
                scope.Dispose();
            }
            var diffChecker = new HashSet<int>();
            Assert.True(cardsInGame.All(diffChecker.Add));
        }
    }
}
