﻿using Dixit_v2.Controllers;
using Dixit_v2.Model;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.GameControllerTests
{
    public class GameControllerRetrieveAllRoundSelectedCardsTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public void CanRetrieveRoundChosenCardsWhenAllCardAreSelected()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<Card>> result;
            var expectedResult = new List<Card>();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int roundId = util.RetrieveValidRoundId();
                var chosenCardIds = _context.RoundChosenCards
                                            .Where(m => m.RoundId == roundId)
                                            .Select(m => m.CardId);
                expectedResult = _context.Cards
                                         .Where(m => chosenCardIds.Contains(m.Id))
                                         .ToList();
                result = sut.RetrieveAllRoundSelectedCards(roundId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualResult = Assert.IsType<List<Card>>(okResult.Value);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void CannotRetrieveRoundChosenCardWhenNotAllCardsForRoundAreSelected()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<Card>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockCompleteGameWithNotAllRoundChosenCards();
                int roundId = util.RetrieveValidRoundId();
                result = sut.RetrieveAllRoundSelectedCards(roundId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<String>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CannotRetrieveRoundChosenCardForInvalidRoundId()
        {
            var sut = new GameController(_context, _loggerFactory, _iConfig);
            ActionResult<IEnumerable<Card>> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameWithAllRoundChosenCards();
                int invalidRoundId = util.GenerateInvalidRoundId();
                result = sut.RetrieveAllRoundSelectedCards(invalidRoundId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<String>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
