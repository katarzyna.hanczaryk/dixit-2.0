﻿namespace Dixit_v2.Model
{
    public class Description
    {
        public string Hint { get; set; }
        public int RoundId { get; set; }
    }
}
