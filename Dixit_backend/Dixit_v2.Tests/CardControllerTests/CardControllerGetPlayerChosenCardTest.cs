﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.CardControllerTests
{
    public class CardControllerGetPlayerChosenCardTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public async Task CanGetCardAddressForValidChosenCardId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<String> result;
            string expectedFilename;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                int validCardId = 1;
                expectedFilename = util.RetrieveCardFilename(validCardId);
                result = await sut.GetPlayerChosenCardAsync(validCardId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualFilename = Assert.IsType<string>(okResult.Value);
            Assert.NotNull(actualFilename);
            Assert.Equal(expectedFilename, actualFilename);
        }

        [Fact]
        public async Task CannotGetCardAddressForInvalidCardId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<String> result;
            string expectedFilename;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                int invalidCardId = util.GenerateInvalidCardId();
                expectedFilename = util.RetrieveCardFilename(invalidCardId);
                result = await sut.GetPlayerChosenCardAsync(invalidCardId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
