﻿namespace Dixit_v2.Common
{
    public class GamePointsSummary
    {
        public int GameId { get; set; }
        public int PlayerId { get; set; }
        public int RoundId { get; set; }
        public int GuessCardId { get; set; }
        public int SelectedCardId { get; set; }
        public int StorytellerId { get; set; }
    }
}
