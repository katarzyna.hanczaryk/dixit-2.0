﻿namespace Dixit_v2.Model
{
    public class FinalPoints
    {
        public string PlayerName { get; set; }
        public int GamePoints { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is FinalPoints)) return false;

            return (this.PlayerName == ((FinalPoints)obj).PlayerName
                && this.GamePoints == ((FinalPoints)obj).GamePoints);
        }

        public override int GetHashCode()
        {
            return PlayerName.GetHashCode()
                ^ GamePoints.GetHashCode();
        }
    }
}
