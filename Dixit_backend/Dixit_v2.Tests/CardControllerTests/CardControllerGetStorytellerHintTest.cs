﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.CardControllerTests
{
    public class CardControllerGetStorytellerHintTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public async Task CanRetrieveExistingHintForValidRoundId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<string> result;
            string expectedHint;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                int roundId = util.RetrieveValidRoundId();
                int cardDescriptionId = util.AddCardDescription(roundId);
                expectedHint = util.RetrieveHint(cardDescriptionId);
                result = await sut.GetStorytellerHintAsync(roundId);
                scope.Dispose();
            }
            var actualResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualRetrievedHint = Assert.IsType<string>(actualResult.Value);
            Assert.NotNull(actualRetrievedHint);
            Assert.Equal(expectedHint, actualRetrievedHint);
        }

        [Fact]
        public async Task CanRetrieveNullWhenHintYetNotAdded()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<string> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                int roundId = util.RetrieveValidRoundId();
                result = await sut.GetStorytellerHintAsync(roundId);
                scope.Dispose();
            }
            var actualResult = Assert.IsType<OkObjectResult>(result.Result);
            Assert.Null(actualResult.Value);
        }

        [Fact]
        public async Task CannotRetrieveHintForInvalidRoundId()
        {
            var sut = new CardController(_context, _loggerFactory, _iConfig);
            ActionResult<string> result;
            string expectedHint;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                int roundId = util.RetrieveValidRoundId();
                int cardDescriptionId = util.AddCardDescription(roundId);
                expectedHint = util.RetrieveHint(cardDescriptionId);
                int invalidRoundId = util.GenerateInvalidRoundId();
                result = await sut.GetStorytellerHintAsync(invalidRoundId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.IsType<string>(failResult.Value);
            Assert.Equal(500, failResult.StatusCode);
        }
    }
}
