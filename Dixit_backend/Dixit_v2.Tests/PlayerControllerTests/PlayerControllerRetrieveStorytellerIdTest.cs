﻿using Dixit_v2.Controllers;
using Dixit_v2.Tests.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Transactions;
using Xunit;

namespace Dixit_v2.Tests.PlayerControllerTests
{
    public class PlayerControllerRetrieveStorytellerIdTest : IntegrationTests
    {
        private Util util = new();
        [Fact]
        public void CanRetrieveStorytellerIdForValidRound()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            int expectedStorytellerId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                var roundId = util.RetrieveValidRoundId();
                expectedStorytellerId = util.RetrieveStorytellerId(roundId);
                result = sut.RetrieveStorytellerId(roundId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualStorytellerId = Assert.IsType<Int32>(okResult.Value);
            Assert.Equal(expectedStorytellerId, actualStorytellerId);
        }

        [Fact]
        public void CannotRetrieveStorytellerIdForInValidRoundId()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.CreatePlayerGameAndRound();
                var invalidRoundId = util.GenerateInvalidRoundId();
                result = sut.RetrieveStorytellerId(invalidRoundId);
                scope.Dispose();
            }
            var failResult = Assert.IsType<ObjectResult>(result.Result);
            Assert.Equal(500, failResult.StatusCode);
        }

        [Fact]
        public void CanRetrieveCorrectStorytellerIdForValidRoundWhenMoreGames()
        {
            var sut = new PlayerController(_context, _loggerFactory, _iConfig);
            ActionResult<int> result;
            int expectedStorytellerId;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                util.MockFullGameBeforeCardsSelection();
                util.CreatePlayerGameAndRound();
                int roundId = util.RetrieveValidRoundId();
                expectedStorytellerId = util.RetrieveStorytellerId(roundId);
                result = sut.RetrieveStorytellerId(roundId);
                scope.Dispose();
            }
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var actualStorytellerId = Assert.IsType<Int32>(okResult.Value);
            Assert.Equal(expectedStorytellerId, actualStorytellerId);
        }
    }
}
