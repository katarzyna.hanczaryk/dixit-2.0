﻿namespace Dixit_v2.Model
{
    public class SelectedCard
    {
        public int ChosenCardId { get; set; }
        public int RoundId { get; set; }
    }
}
